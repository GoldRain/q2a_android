package com.q2a.activitys;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.adapter.ShowDetailsPreviewAdapter;
import com.q2a.adapter.TimelineImageAdapter;
import com.q2a.adapter.TimelineVideoEditAdapter;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.QuestionEntity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jp.shts.android.library.TriangleLabelView;

public class ShowDetailsActivity extends CommonActivity implements View.OnClickListener {

    TextView txv_title, txv_description, txv_subject, txv_delete, txv_edit, txv_resubmit, txv_call ,txv_accept, txv_reject,txv_novideo, txv_noimages;
    RecyclerView rec_images;

    ImageView imv_video, imv_videoplay;

    TimelineVideoEditAdapter _adapter_video;
    TimelineImageAdapter _adapter_images;

    ShowDetailsPreviewAdapter _adapter;

    QuestionEntity _question = new QuestionEntity();
    ArrayList<String> _imagePaths = new ArrayList<>();
    ArrayList<String> _images = new ArrayList<>();
    String _videoPaths = "";
    String _videoThumbPaths = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);

        Constants.FROM_NOTIFICATION = 0;
        _question = (QuestionEntity) getIntent().getSerializableExtra("questions");

        loadLayout();
    }

    private void loadLayout() {

        ImageView imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMain();
            }
        });

        txv_title = (TextView)findViewById(R.id.txv_title);
        txv_description = (TextView)findViewById(R.id.txv_description);
        txv_subject = (TextView)findViewById(R.id.txv_subject);

        txv_delete = (TextView)findViewById(R.id.txv_delete);
        txv_delete.setOnClickListener(this);

        txv_edit = (TextView)findViewById(R.id.txv_edit);
        txv_edit.setOnClickListener(this);

        txv_resubmit = (TextView)findViewById(R.id.txv_resubmit);
        txv_resubmit.setOnClickListener(this);

        txv_call = (TextView)findViewById(R.id.txv_call);
        txv_call.setOnClickListener(this);

        rec_images = (RecyclerView)findViewById(R.id.recycler_image);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL, false);
        rec_images.setLayoutManager(layoutManager1);

        _adapter = new ShowDetailsPreviewAdapter(this);
        rec_images.setAdapter(_adapter);

        imv_video = (ImageView)findViewById(R.id.imv_video);
        imv_video.setOnClickListener(this);
        imv_videoplay = (ImageView)findViewById(R.id.imv_videoplay);
        txv_novideo = (TextView)findViewById(R.id.txv_novideo);

        txv_noimages = (TextView)findViewById(R.id.txv_noimages);

        txv_delete.setVisibility(View.VISIBLE);
        txv_edit.setVisibility(View.VISIBLE);
        txv_resubmit.setVisibility(View.VISIBLE);
        txv_call.setVisibility(View.VISIBLE);

        showDetails();

    }

    private void gotoMain(){

        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    private void showDetails(){

        Log.d("=====Status==>", String.valueOf(_question.get_status()));

        TriangleLabelView label_view = (TriangleLabelView) findViewById(R.id.label_view);

        if (_question.get_status() == 0){   // 0 : request :

            label_view.setTriangleBackgroundColor(getResources().getColor(R.color.blue));
            label_view.setPrimaryText("Request");

            if (Commons.g_user.get_subscriber() == 1){

                txv_resubmit.setVisibility(View.VISIBLE);
                txv_resubmit.setText("Accept");
                txv_delete.setVisibility(View.VISIBLE);
                txv_delete.setText("Reject");

                txv_edit.setVisibility(View.GONE);
                txv_call.setVisibility(View.GONE);

            } else {

                txv_delete.setVisibility(View.GONE);
                txv_edit.setVisibility(View.GONE);
                txv_resubmit.setVisibility(View.GONE);
                txv_call.setVisibility(View.GONE);
            }

        } else if (_question.get_status() == 1){   //1: accepted :

            txv_delete.setVisibility(View.GONE);
            txv_edit.setVisibility(View.GONE);
            txv_resubmit.setVisibility(View.GONE);

            txv_call.setVisibility(View.VISIBLE);

            label_view.setTriangleBackgroundColor(getResources().getColor(R.color.green));
            label_view.setPrimaryText("Accepted");

        } else if (_question.get_status() == 2){  //2: rejected:

            txv_delete.setVisibility(View.VISIBLE);
            txv_edit.setVisibility(View.VISIBLE);
            txv_resubmit.setVisibility(View.VISIBLE);

            txv_call.setVisibility(View.GONE);

            label_view.setTriangleBackgroundColor(getResources().getColor(R.color.red));
            label_view.setPrimaryText("Rejected");
        }

        if (_question.get_thumb_url().length() > 0 && _question.get_video_url().length() > 0){

            imv_videoplay.setVisibility(View.VISIBLE);
            txv_novideo.setVisibility(View.GONE);

        } else {

            imv_videoplay.setVisibility(View.GONE);
            txv_novideo.setVisibility(View.VISIBLE);
        }

        if (_question.get_images_url().size() > 0){

            txv_noimages.setVisibility(View.GONE);

        } else txv_noimages.setVisibility(View.VISIBLE);


        txv_title.setText(_question.get_title());
        txv_description.setText(_question.get_description());

        if (_question.get_subjects().size() > 0) txv_subject.setText(_question.get_subjects().get(0));

        Glide.with(this).load(_question.get_thumb_url()).placeholder(R.color.transparent).into(imv_video);

        for (int i = 0; i < _question.get_images_url().size(); i++){

            _images.add(_question.get_images_url().get(i).get_image_url());
        }

        addImages(_images);
    }

    public void addImages(ArrayList<String> paths) {

        _imagePaths.clear();

        _imagePaths.addAll(paths);
        _adapter.setDatas(_imagePaths);
    }

    private void videoPlay(){

        Intent intent = new Intent(_context, VideoPreviewActivity.class);
        if (_question.get_video_url().length() > 0) intent.putExtra(Constants.KEY_VIDEOPATH, _question.get_video_url());
        _context.startActivity(intent);

    }

    private void DeleteRequest(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_DELETE_REQUEST;

        showProgress();
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                closeProgress();

                Log.d("delete===response",json);

                try {
                    JSONObject response = new JSONObject(json);

                    int result_code = response.getInt(ReqConst.RES_CODE);
                    if (result_code == ReqConst.CODE_SUCCESS){

                        gotoMain();
                    }
                } catch (JSONException e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_question.get_id()));
                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);


    }

    private void AcceptRequest(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ACCEPT_REQUEST;

        showProgress();
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                closeProgress();

                Log.d("AcceptRequest=response",json);

                try {
                    JSONObject response = new JSONObject(json);

                    int result_code = response.getInt(ReqConst.RES_CODE);
                    if (result_code == ReqConst.CODE_SUCCESS){

                        gotoMain();
                    }
                } catch (JSONException e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_question.get_id()));
                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);


    }

    private void RejectRequest(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_REJECT_REQUEST;

        showProgress();
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                closeProgress();

                Log.d("Reject===response",json);

                try {
                    JSONObject response = new JSONObject(json);

                    int result_code = response.getInt(ReqConst.RES_CODE);
                    if (result_code == ReqConst.CODE_SUCCESS){

                        gotoMain();
                    }
                } catch (JSONException e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_question.get_id()));
                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);


    }

    private void ResubmitRequest(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_RESUBMIT_REQUEST;

        showProgress();
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                closeProgress();

                Log.d("resubmit===response",json);

                try {

                    JSONObject response = new JSONObject(json);

                    int result_code = response.getInt(ReqConst.RES_CODE);
                    if (result_code == ReqConst.CODE_SUCCESS){

                        gotoMain();
                    }
                } catch (JSONException e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(_question.get_id()));
                    params.put(ReqConst.PARAM_TITLE, txv_title.getText().toString());
                    params.put(ReqConst.PARAM_DESCRIPTION, txv_description.getText().toString());
                    params.put(ReqConst.PARAM_SUBJECTS, txv_subject.getText().toString());

                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);
    }

    private void callPhone() {

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData( Uri.parse("tel:" + _question.get_other_phone()));

        Log.d(_question.get_other_phone(), "Phone==number==>");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_video:
                videoPlay();
                break;

            case R.id.txv_delete:

                if (Commons.g_user.get_subscriber() == 1){

                    RejectRequest();

                } else {
                    DeleteRequest();
                }
                break;

            case R.id.txv_edit:
                Constants.FROM_NOTIFICATION = 200; //to Postquestio Activity variable
                Intent intent = new Intent(this, PostQuestionActivity.class);
                intent.putExtra(Constants.KEY_QUESTION, _question);
                startActivity(intent);
                finish();
                break;

            case R.id.txv_resubmit:

                if (Commons.g_user.get_subscriber() == 1){
                    AcceptRequest();
                } else {
                    ResubmitRequest();
                }
                break;

            case R.id.txv_call:
                callPhone();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        gotoMain();
    }
}
