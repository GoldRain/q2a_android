package com.q2a.activitys;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.q2a.R;
import com.q2a.Utils.RadiusImageView;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.fragments.SubscriberFragment;
import com.q2a.fragments.HomeFragment;
import com.q2a.fragments.NotificationFragment;
import com.q2a.fragments.ProfileFragment;
import com.q2a.fragments.subscriber.Sub_HomeFragment;
import com.q2a.fragments.subscriber.Sub_NotiFragment;
import com.q2a.fragments.subscriber.Sub_ProfileFragment;
import com.q2a.model.QuestionEntity;
import com.q2a.model.UserEntity;
import com.q2a.preference.PrefConst;
import com.q2a.preference.Preference;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static com.q2a.R.id.fly_container;

public class MainActivity extends CommonActivity implements View.OnClickListener {

    DrawerLayout ui_drawerlayout;

    ImageView ui_imv_call_draw, imv_setting;
    RadiusImageView ui_imvPhoto;
    TextView txv_name;

    LinearLayout lyt_menu_header;
    LinearLayout lyt_home_nav, lyt_subscriber_nav, lyt_notification_nav, lyt_profile_nav, lyt_logout_nav;   //nav
    LinearLayout lyt_home_tab, lyt_subsriber_tab, lyt_notification_tab, lyt_profile_tab;  //tab

    ImageView imv_home, imv_subscriber, imv_noti, imv_profile, imv_home_nav, imv_subscriber_nav, imv_noti_nav, imv_profile_nav;
    TextView txv_home, txv_subscriber, txv_noti, txv_profile, txv_home_nav, txv_subscriber_nav, txv_noti_nav, txv_badge_tab,txv_badge_nav, txv_profile_nav;
    TextView txv_title;
    TextView txv_unread,txv_unread_nav;

    MenuItem menu_rejected;

    ArrayList<QuestionEntity> _notifications = new ArrayList<>();
    ArrayList<QuestionEntity> _request_array = new ArrayList<>();

    String[] strings = null;
    MaterialSpinner spinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txv_badge_tab = (TextView) findViewById(R.id.txv_unread);
        txv_badge_nav = (TextView) findViewById(R.id.txv_unread_nav);

        Constants.mainActivity = this;
        checkLocationPermission();

        loadLayout();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((firsttapreceiver),
                new IntentFilter("firsttap")
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(firsttapreceiver);
    }

    private BroadcastReceiver firsttapreceiver = new BroadcastReceiver() {   // request service request
        @Override
        public void onReceive(final Context context, Intent intent) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    txv_badge_tab.setVisibility(View.VISIBLE);
                    txv_badge_tab.setText(String.valueOf(Constants.NOTI_COUNTER));

                    txv_badge_nav.setVisibility(View.VISIBLE);
                    txv_badge_nav.setText(String.valueOf(Constants.NOTI_COUNTER));

                    if (Constants.SUB_HOME_FRAGMENT != null){

                        Constants.SUB_HOME_FRAGMENT.getAllHome();

                    } else if (Constants.SUB_NOTI_FRAGMENT != null){

                        Constants.SUB_NOTI_FRAGMENT.getAllNotification();

                    }  else if (Constants.STUDENT_HOME_FRAGMENT != null){

                        Constants.STUDENT_HOME_FRAGMENT.getAllHome();

                    } else if (Constants.STUDENT_NOTI_FRAGMENT != null){

                        Constants.STUDENT_NOTI_FRAGMENT.getAllNotification1();
                    }
                }

            });
        }


    };

    private void loadLayout() {

        ui_imvPhoto = (RadiusImageView)findViewById(R.id.imv_photo_h);
        txv_name = (TextView)findViewById(R.id.txv_name);

        Glide.with(this).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.bg_non_profile).into(ui_imvPhoto);
        txv_name.setText(Commons.g_user.get_full_name());

        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);

        ui_imv_call_draw = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imv_call_draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDrawer();
            }
        });

        imv_setting = (ImageView)findViewById(R.id.imv_setting);
        imv_setting.setOnClickListener(this);

        lyt_menu_header = (LinearLayout)findViewById(R.id.lyt_menu_header);
        lyt_menu_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProfileFragment();
                ui_drawerlayout.closeDrawers();
            }
        });

    /*NAVIGATION*/
        lyt_home_nav = (LinearLayout)findViewById(R.id.lyt_home_nav);
        lyt_home_nav.setOnClickListener(this);

        lyt_subscriber_nav = (LinearLayout)findViewById(R.id.lyt_subscriber_nav);
        lyt_subscriber_nav.setOnClickListener(this);

        lyt_notification_nav = (LinearLayout)findViewById(R.id.lyt_noti_nav);
        lyt_notification_nav.setOnClickListener(this);

        lyt_profile_nav = (LinearLayout)findViewById(R.id.lyt_profile_nav);
        lyt_profile_nav.setOnClickListener(this);

        lyt_logout_nav = (LinearLayout)findViewById(R.id.lyt_logout_nav);
        lyt_logout_nav.setOnClickListener(this);

    /*TAB*/

        lyt_home_tab = (LinearLayout)findViewById(R.id.lyt_home);
        lyt_home_tab.setOnClickListener(this);

        lyt_subsriber_tab = (LinearLayout)findViewById(R.id.lyt_subscriber);
        lyt_subsriber_tab.setOnClickListener(this);

        lyt_notification_tab = (LinearLayout)findViewById(R.id.lyt_noti);
        lyt_notification_tab.setOnClickListener(this);

        lyt_profile_tab = (LinearLayout)findViewById(R.id.lyt_profile);
        lyt_profile_tab.setOnClickListener(this);

        //TAB_IMAGE
        imv_home = (ImageView)findViewById(R.id.imv_home);
        imv_subscriber = (ImageView)findViewById(R.id.imv_subscriber);
        imv_noti = (ImageView)findViewById(R.id.imv_noti);
        imv_profile = (ImageView)findViewById(R.id.imv_profile);

        //TAB_TEXT
        txv_home = (TextView)findViewById(R.id.txv_home);
        txv_subscriber = (TextView)findViewById(R.id.txv_subscriber);
        txv_noti = (TextView)findViewById(R.id.txv_noti);
        txv_profile = (TextView)findViewById(R.id.txv_profile);

        //NAV_IMAGE
        imv_home_nav = (ImageView)findViewById(R.id.imv_home_nav);
        imv_subscriber_nav = (ImageView)findViewById(R.id.imv_subscriber_nav);
        imv_noti_nav = (ImageView)findViewById(R.id.imv_noti_nav);
        imv_profile_nav = (ImageView)findViewById(R.id.imv_profile_nav);

        //NAV_TEXT
        txv_home_nav = (TextView)findViewById(R.id.txv_home_nav);
        txv_subscriber_nav = (TextView)findViewById(R.id.txv_subscriber_nav);
        txv_noti_nav = (TextView)findViewById(R.id.txv_noti_nav);
        txv_profile_nav = (TextView)findViewById(R.id.txv_profile_nav);

        txv_title = (TextView)findViewById(R.id.txv_title);

        txv_unread = (TextView)findViewById(R.id.txv_unread);
        txv_unread_nav = (TextView)findViewById(R.id.txv_unread_nav);

        spinner = (MaterialSpinner)findViewById(R.id.spinner);

        if (Constants.EDIT_PROFILE == 1){

            Constants.EDIT_PROFILE = 0;
            gotoProfileFragment();

        } else {

            Constants.EDIT_PROFILE = 0;
            gotoHomeFragment();
        }

        ui_drawerlayout.closeDrawers();

        //if user is a subscriber or student
        if (Commons.g_user.get_subscriber() == 1 ){

            lyt_subsriber_tab.setVisibility(View.GONE);
            lyt_subscriber_nav.setVisibility(View.GONE);

        } else {

            lyt_subsriber_tab.setVisibility(View.VISIBLE);
            lyt_subscriber_nav.setVisibility(View.VISIBLE);
        }


        if (Commons.g_user.get_subscriber() == 0){
            strings = new String[]{"      All", "Requested", "Accepted", "Rejected"};
        } else {
            strings = new String[]{"      All", "Requested", "Accepted"};}


        spinner.setItems(strings);
        final String[] producttype = {strings[0]};
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                producttype[0] = item;

                Log.d("item===", item);

                    //0: all: 1: request: 2:accepted:3:rejected

                if (item.equals("      All")){

                    Constants.FILTER_STATUS = 0;
                    gotoHomeFragment();

                } else if (item.equals("Requested")){

                    Constants.FILTER_STATUS = 1;
                    gotoHomeFragment();

                } else if (item.equals("Accepted")) {


                    Constants.FILTER_STATUS = 2;
                    gotoHomeFragment();

                } else if (item.equals("Rejected")) {

                    Constants.FILTER_STATUS = 3;
                    gotoHomeFragment();
                }
            }
        });

        showBadges();
    }

    public void showBadges(){

        if (Constants.NOTI_COUNTER != 0) {

            txv_badge_tab.setVisibility(View.VISIBLE);
            txv_badge_tab.setText(String.valueOf(Constants.NOTI_COUNTER));

            txv_badge_nav.setVisibility(View.VISIBLE);
            txv_badge_nav.setText(String.valueOf(Constants.NOTI_COUNTER));

        } else {

            txv_badge_tab.setVisibility(View.GONE);
            txv_badge_nav.setVisibility(View.GONE);
        }
    }

    public void showDrawer() {

        DrawerLayout ui_drawerlayout =(DrawerLayout)findViewById(R.id.drawerlayout);

        ui_drawerlayout.openDrawer(Gravity.LEFT);
    }

    public void gotoHomeFragment(){

        txv_title.setText("Home");
        imv_setting.setVisibility(View.VISIBLE);
        spinner.setVisibility(View.VISIBLE);

        if (Constants.FILTER_STATUS == 0) spinner.setSelectedIndex(0);

        setTab(R.drawable.ic_home_pos, R.drawable.ic_subscriber_neg, R.drawable.ic_noti_neg, R.drawable.ic_profile_neg,
                R.drawable.ic_home_pos, R.drawable.ic_subscriber_neg, R.drawable.ic_noti_neg, R.drawable.ic_profile_neg);
        setTabText(getResources().getColor(R.color.primary_dark), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text),
                getResources().getColor(R.color.primary_dark), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text));

        /* false:student, true:subscriber */
        if (Commons.g_user.get_subscriber() == 1){

            Sub_HomeFragment fragment = new Sub_HomeFragment(this);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(fly_container, fragment).commit();

        }else {
            HomeFragment fragment = new HomeFragment(this);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(fly_container, fragment).commit();
        }
    }

    public void gotoSubscriberFragment(){

        txv_title.setText("Subscriber");
        imv_setting.setVisibility(View.GONE);
        spinner.setVisibility(View.GONE);

        Constants.FILTER_STATUS = 0;

        setTab(R.drawable.ic_home_neg, R.drawable.ic_subscriber_pos, R.drawable.ic_noti_neg, R.drawable.ic_profile_neg,
                R.drawable.ic_home_neg, R.drawable.ic_subscriber_pos, R.drawable.ic_noti_neg, R.drawable.ic_profile_neg);
        setTabText(getResources().getColor(R.color.tab_text), getResources().getColor(R.color.primary_dark), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text),
                getResources().getColor(R.color.tab_text), getResources().getColor(R.color.primary_dark), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text));

        SubscriberFragment fragment = new SubscriberFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(fly_container, fragment).commit();
    }

    public void gotoNotificationFragment(){

        txv_title.setText("Notification");
        imv_setting.setVisibility(View.GONE);
        spinner.setVisibility(View.GONE);

        Constants.FILTER_STATUS = 0;

        setTab(R.drawable.ic_home_neg, R.drawable.ic_subscriber_neg, R.drawable.ic_noti_pos, R.drawable.ic_profile_neg,
                R.drawable.ic_home_neg, R.drawable.ic_subscriber_neg, R.drawable.ic_noti_pos, R.drawable.ic_profile_neg);
        setTabText(getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.primary_dark), getResources().getColor(R.color.tab_text),
                getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.primary_dark), getResources().getColor(R.color.tab_text));

        if (Commons.g_user.get_subscriber() == 1){
            Sub_NotiFragment fragment = new Sub_NotiFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(fly_container, fragment).commit();

        } else {
            NotificationFragment fragment = new NotificationFragment(this);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(fly_container, fragment).commit();
        }
    }

    public void gotoProfileFragment(){

        txv_title.setText("Profile");
        imv_setting.setVisibility(View.GONE);
        spinner.setVisibility(View.GONE);

        setTab(R.drawable.ic_home_neg, R.drawable.ic_subscriber_neg, R.drawable.ic_noti_neg, R.drawable.ic_profile_pos,
                R.drawable.ic_home_neg, R.drawable.ic_subscriber_neg, R.drawable.ic_noti_neg, R.drawable.ic_profile_pos);
        setTabText(getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.primary_dark),
                getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.tab_text), getResources().getColor(R.color.primary_dark));

        Constants.FILTER_STATUS = 0;

        if (Commons.g_user.get_subscriber() == 1){

            Sub_ProfileFragment fragment = new Sub_ProfileFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(fly_container, fragment).commit();

        } else {

            ProfileFragment fragment = new ProfileFragment(this);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(fly_container, fragment).commit();
        }
    }

    public void gotoProfileEdit(){

        startActivity(new Intent(this, ProfileEditActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    public void gotoSubEditProfile(){

        startActivity(new Intent(this, SubEditProfileActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    public void gotoSubscriberDetails(UserEntity subscriber){

        Intent intent = new Intent(this, SubscribersDetailsActivity.class);
        intent.putExtra("subscriber", subscriber);
        overridePendingTransition(0,0);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void gotoShowDetails(QuestionEntity questionEntity){

        Intent intent = new Intent(this, ShowDetailsActivity.class);
        intent.putExtra("questions", questionEntity);
        overridePendingTransition(0,0);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void gotoPostQuestion(QuestionEntity entity){

        Constants.FROM_NOTIFICATION = 200;
        Intent intent = new Intent(this, PostQuestionActivity.class);
        intent.putExtra(Constants.KEY_QUESTION, entity);
        overridePendingTransition(0,0);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void setTab(int a, int b, int c, int d, int e, int f, int g, int h){

        imv_home.setImageResource(a);
        imv_subscriber.setImageResource(b);
        imv_noti.setImageResource(c);
        imv_profile.setImageResource(d);
        //NAV
        imv_home_nav.setImageResource(e);
        imv_subscriber_nav.setImageResource(f);
        imv_noti_nav.setImageResource(g);
        imv_profile_nav.setImageResource(h);
    }
    public void setTabText(int a, int b, int c, int d, int e, int f, int g, int h ){

        txv_home.setTextColor(a);
        txv_subscriber.setTextColor(b);
        txv_noti.setTextColor(c);
        txv_profile.setTextColor(d);

        //NAV
        txv_home_nav.setTextColor(e);
        txv_subscriber_nav.setTextColor(f);
        txv_noti_nav.setTextColor(g);
        txv_profile_nav.setTextColor(h);

    }

    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem register = menu.findItem(R.id.item_rejected);
        if(Commons.g_user.get_subscriber() == 1)
        {
            register.setVisible(false);
        }
        else
        {
            register.setVisible(true);
        }
        return true;
    }

    private void showSettingMenu(View v){

        android.widget.PopupMenu popupMenu = new android.widget.PopupMenu(MainActivity.this, v);
        popupMenu.inflate(R.menu.popup_setting_menu);

        Object menuHelper;
        Class[] argTypes;

        try {
            Field fMenuHelper = android.widget.PopupMenu.class.getDeclaredField("mPopup");

            fMenuHelper.setAccessible(true);
            menuHelper = fMenuHelper.get(popupMenu);
            argTypes = new Class[]{boolean.class};
            menuHelper.getClass().getDeclaredMethod("setForceShowIcon", argTypes).invoke(menuHelper, true);

        } catch (Exception e) {

            Log.w("Error====>", "error forcing menu icons to show", e);
            popupMenu.show();

            return;
        }
        popupMenu.setOnMenuItemClickListener(new android.widget.PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                switch (menuItem.getItemId()) {

                    //0: all: 1: request: 2:accepted:3:rejected
                    case R.id.item_all:

                        Constants.FILTER_STATUS = 0;
                        gotoHomeFragment();
                        return true;

                    case R.id.item_requested:
                        Constants.FILTER_STATUS = 1;
                        gotoHomeFragment();
                        return true;

                    case R.id.item_accepted:
                        Constants.FILTER_STATUS = 2;
                        gotoHomeFragment();
                        return true;

                    case R.id.item_rejected:
                        Constants.FILTER_STATUS = 3;
                        gotoHomeFragment();
                        return true;
                }

                return true;
            }
        });

        popupMenu.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lyt_home_nav:
                gotoHomeFragment();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.imv_setting:
                showSettingMenu(v);
                break;

            case R.id.lyt_subscriber_nav:
                gotoSubscriberFragment();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_noti_nav:
                gotoNotificationFragment();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_profile_nav:
                gotoProfileFragment();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_logout_nav:

                startActivity(new Intent(this, SignInActivity.class));

                Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME, "");
                Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, "");
                finish();
                break;

            case R.id.lyt_home:
                gotoHomeFragment();
                break;

            case R.id.lyt_subscriber:
                gotoSubscriberFragment();
                break;

            case R.id.lyt_noti:
                gotoNotificationFragment();
                break;

            case R.id.lyt_profile:
                gotoProfileFragment();
                break;
        }
    }

    public void decreasebage(){

        Constants.NOTI_COUNTER--;

        txv_unread.setText(String.valueOf(Constants.NOTI_COUNTER));
        txv_unread_nav.setText(String.valueOf(Constants.NOTI_COUNTER));
    }

    public static boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {

                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void checkLocationPermission() {

        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE ,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.CAPTURE_VIDEO_OUTPUT};

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }

    }

    @Override
    public void onBackPressed() {

        if (ui_drawerlayout.isDrawerOpen(GravityCompat.START)){

            ui_drawerlayout.closeDrawer(GravityCompat.START);

        } else onExit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.mainActivity = null;
    }

}
