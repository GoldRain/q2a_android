package com.q2a.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.preference.PrefConst;
import com.q2a.preference.Preference;

import net.igenius.customcheckbox.CustomCheckBox;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends CommonActivity  implements View.OnClickListener{

    TextView txv_check_teacher, txv_signup;
    EditText edt_email, edt_firstName, edt_lastName,edt_phoneNumber, edt_password, edt_confirmPwd;

    View view;
    CustomCheckBox scb;
    int _subscriber = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        loadLayout();
    }

    private void loadLayout() {

        edt_email = (EditText)findViewById(R.id.edt_email);
        edt_firstName = (EditText)findViewById(R.id.edt_firstName);
        edt_lastName = (EditText)findViewById(R.id.edt_lastName);
        edt_phoneNumber = (EditText)findViewById(R.id.edt_phoneNumber);
        edt_password = (EditText)findViewById(R.id.edt_password);
        edt_confirmPwd = (EditText)findViewById(R.id.edt_confirmPwd);

        txv_signup = (TextView)findViewById(R.id.txv_signup);
        txv_signup.setOnClickListener(this);

        txv_check_teacher = (TextView)findViewById(R.id.txv_check_teacher);

        scb = (CustomCheckBox) findViewById(R.id.check_subscriber);
        scb.setOnCheckedChangeListener(new CustomCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomCheckBox checkBox, boolean isChecked) {

                if (scb.isChecked()){

                    txv_check_teacher.setTextColor(getResources().getColor(R.color.red));
                    //scb.setChecked(false);
                    _subscriber = 1;

                } else {

                    _subscriber = 0;
                    Log.d("CustomCheckBox", String.valueOf(isChecked));
                    txv_check_teacher.setTextColor(getResources().getColor(R.color.gray));

                }

            }
        });

        LinearLayout lyt_signup = (LinearLayout)findViewById(R.id.lyt_signup);
        lyt_signup.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(),0);

                return false;
            }
        });
    }

    private boolean checkValid(){

        if (edt_email.getText().toString().length() == 0){

            showAlertDialog("Please input your email");
            return false;

        } else if (edt_firstName.getText().toString().length() == 0){

            showAlertDialog("Please input your first name");
            return false;

        } else if (edt_lastName.getText().toString().length() == 0){

            showAlertDialog("Please input your last name");
            return false;

        } else if (edt_phoneNumber.getText().toString().length() == 0){

            showAlertDialog("Please input your phone number");
            return false;

        } else if (edt_password.getText().toString().length() == 0){

            showAlertDialog("Please input your password");
            return false;

        } else if (!edt_password.getText().toString().equals(edt_confirmPwd.getText().toString())){

            showAlertDialog("Please correct password");
            return false;
        }
        return true;
    }

    public void gotoMain(View view){

        startActivity(new Intent(this, AlertActivity.class));
        overridePendingTransition(0,0);
        finish();

    }

    public void gotoSignIn(View view){

        startActivity(new Intent(this, SignInActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    private void progressSingUp(){

        showProgress();


        String url = ReqConst.SERVER_URL + ReqConst.REQ_SINGUP;
        Log.d("======Signup==URL==>", url);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSignUp(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_FIRST_NAME, edt_firstName.getText().toString());
                    params.put(ReqConst.PARAM_LAST_NAME, edt_lastName.getText().toString());
                    params.put(ReqConst.PARAM_EMAIL, edt_email.getText().toString());
                    params.put(ReqConst.PARAM_PASSWORD, edt_password.getText().toString());
                    params.put(ReqConst.PARAM_PHONE, edt_phoneNumber.getText().toString());
                    params.put(ReqConst.PARAM_SUBSCRIBER, String.valueOf(_subscriber));

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseSignUp(String json) {

        Log.d("====SignupResponse=>", json);

        closeProgress();

        try {

            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                gotoSignUp();

            } else if (result_code == 101){

                showAlertDialog("Already exist email or phone number.");
            }

        } catch (JSONException e) {
            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

    public void gotoSignUp(){

        closeProgress();

        Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME, edt_phoneNumber.getText().toString().trim());
        Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, edt_password.getText().toString().trim());
        Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERNAME, "");

        setResult(RESULT_OK);

        startActivity(new Intent(this, SignInActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_signup:
                if (checkValid()){
                    progressSingUp();
                    break;
                }
        }

    }
}
