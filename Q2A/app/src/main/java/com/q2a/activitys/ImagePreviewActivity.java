package com.q2a.activitys;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.q2a.R;
import com.q2a.adapter.ImagePreviewAdapter;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Constants;

import java.util.ArrayList;

public class ImagePreviewActivity extends CommonActivity implements View.OnClickListener {

    ViewPager ui_viewPager;
    ImagePreviewAdapter _adapter;
    TextView ui_txvImageNo;

    ArrayList<String> _imagePaths = new ArrayList<>();
    int _position = 0;
    String _comment = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        _imagePaths = getIntent().getStringArrayListExtra(Constants.KEY_IMAGEPATH);
        _position = getIntent().getIntExtra(Constants.KEY_POSITION,0);
        _comment = getIntent().getStringExtra(Constants.KEY_COMMENT);

        loadLayout();
    }

    private void loadLayout(){

        ui_txvImageNo = (TextView)findViewById(R.id.txv_timeline_no);


        ImageView imvBack = (ImageView)findViewById(R.id.imv_back);
        imvBack.setOnClickListener(this);

        ui_viewPager = (ViewPager)findViewById(R.id.viewpager);
        _adapter = new ImagePreviewAdapter(this);
        ui_viewPager.setAdapter(_adapter);
        _adapter.setDatas(_imagePaths);
        ui_viewPager.setCurrentItem(_position);
        ui_txvImageNo.setText((_position + 1) + " / " + _imagePaths.size());

        ui_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ui_txvImageNo.setText((position + 1) + " / " + _imagePaths.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;
        }
    }

}
