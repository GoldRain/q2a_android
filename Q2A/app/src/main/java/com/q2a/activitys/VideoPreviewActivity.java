package com.q2a.activitys;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.q2a.R;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Constants;


public class VideoPreviewActivity extends CommonActivity implements View.OnClickListener {

    VideoView ui_videoView;

    String _videoPath = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_preview);

        _videoPath = getIntent().getStringExtra(Constants.KEY_VIDEOPATH);

        loadLayout();
    }

    private void loadLayout() {

        TextView txvCancel = (TextView)findViewById(R.id.txv_cancel);
        txvCancel.setOnClickListener(this);

        ImageView imvPlay = (ImageView)findViewById(R.id.imv_play);
        imvPlay.setOnClickListener(this);


        ui_videoView = (VideoView)findViewById(R.id.videoview);

        try {
            ui_videoView.setMediaController(null);
            ui_videoView.requestFocus();
            ui_videoView.setVideoURI(Uri.parse(_videoPath));
            ui_videoView.seekTo(1);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void playVideo(){

        ui_videoView.seekTo(0);
        ui_videoView.start();
    }

    private void onBack(){ finish();}

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_cancel:
                onBack();
                break;

            case R.id.imv_play:
                playVideo();
                break;

        }

    }
}
