package com.q2a.activitys;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.adapter.Sub_HomeListViewAdapter;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.QuestionEntity;
import com.q2a.model.UserEntity;
import com.q2a.preference.PrefConst;
import com.q2a.preference.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SignInActivity extends CommonActivity implements View.OnClickListener {

    EditText edt_email_phone, edt_password;
    TextView txv_signin,txv_facebook_sign;
    LinearLayout lyt_signup;

    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.RECORD_AUDIO, Manifest.permission.BIND_VOICE_INTERACTION, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS};

    int MY_PEQUEST_CODE = 123;
    public static final int REGISTER_CODE = 100;

    ArrayList<QuestionEntity> _allQuestion = new ArrayList<>();

    // facebook login
    public static CallbackManager callbackManager;
    private String FEmail, Name,Firstname, Lastname,Id,Gender,Image_url;
    String photourl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        checkAllPermission();
        initValue();
        loadLayout();


        try
        {

            PackageInfo info = getPackageManager().getPackageInfo("com.q2a", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");

                md.update(signature.toByteArray());
                Log.i("KeyHash::", Base64.encodeToString(md.digest(), Base64.DEFAULT));//will give developer key hash
//                Toast.makeText(getApplicationContext(), Base64.encodeToString(md.digest(), Base64.DEFAULT), Toast.LENGTH_LONG).show(); //will give app key hash or release key hash

            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {}
    }

    /*==================== Permission========================================*/
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    /*//////////////////////////////////////////////////////////////////////////////*/

    public boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PEQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }

    }

    private void initValue() {

        Intent intent = getIntent();

        try {
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);

        } catch (Exception e){
        }
    }

    boolean _isFromLogout = false ;

    private void loadLayout() {

        edt_email_phone = (EditText)findViewById(R.id.edt_email_phone);
        edt_password = (EditText)findViewById(R.id.edt_password);

        txv_signin = (TextView)findViewById(R.id.txv_signin);
        txv_signin.setOnClickListener(this);

        txv_facebook_sign = (TextView)findViewById(R.id.txv_facebook_sign);
        txv_facebook_sign.setOnClickListener(this);

        LinearLayout lyt_container = (LinearLayout)findViewById(R.id.activity_signin);
        lyt_container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email_phone.getWindowToken(),0);
                return false;
            }
        });

        if (_isFromLogout){

            //   save user to empty
            Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME,"");
            Preference.getInstance().put(this,PrefConst.PREFKEY_USERPWD, "");

            edt_email_phone.setText("");
            edt_password.setText("");

        } else {

            String _name = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERNAME, "");
            String _password = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERPWD, "");

            edt_email_phone.setText(_name);
            edt_password.setText(_password);

            if ( _name.length()>0 && _password.length() > 0 ) {

                processLogin();
            }
        }
    }

    public void processLogin(){


        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;

        Log.d("URL=========", url);

        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                parseLoginResponse(response);
            }

        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        closeProgress();
                        showAlertDialog(getString(R.string.error));
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.RES_EMAIL, edt_email_phone.getText().toString());
                    params.put(ReqConst.RES_PASSWORD, edt_password.getText().toString());

                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseLoginResponse(String json) {

        Log.d("====LoginResponse==>", json);

        try {
            closeProgress();

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                UserEntity user = new UserEntity();

                JSONObject user_info = response.getJSONObject(ReqConst.RES_USER_INFO);

                user.set_id(user_info.getInt(ReqConst.RES_USER_ID));
                user.set_email(user_info.getString(ReqConst.RES_EMAIL));
                user.set_first_name(user_info.getString(ReqConst.RES_FIRST_NAME));
                user.set_last_name(user_info.getString(ReqConst.RES_LAST_NAME));
                user.set_phone_number(user_info.getString(ReqConst.RES_PHONE));
                user.set_photoUrl(user_info.getString(ReqConst.RES_PHOTO_URL));

                if (user.get_subscriber() == 1){
                    ArrayList<String> _subjects = new ArrayList<String>(Arrays.asList(user_info.getString(ReqConst.RES_SUBJECTS).split(":")));
                    user.set_subjects(_subjects);
                }

                user.set_subscriber(user_info.getInt(ReqConst.RES_SUBSCRIBER));
                //case subscriber exist the description
                if (user.get_subscriber() == 1)
                    user.set_description(user_info.getString(ReqConst.RES_DESCRIPTION));

                user.set_payment(user_info.getInt(ReqConst.RES_PAYMENT));
                Constants.NOTI_COUNTER = (user_info.getInt(ReqConst.RES_NOTI_CNT));

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERNAME, edt_email_phone.getText().toString().trim());

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERPWD, edt_password.getText().toString().trim());

                Commons.g_user = user;

                Constants.TOKEN = Preference.getInstance().getValue(this, PrefConst.PREFKEY_TOKEN, "");
                if ( Constants.TOKEN.toString().length() > 0){

                    updateToken();

                }else  gotoMain();

            } else if (result_code == ReqConst.CODE_UNREGISTER){

                closeProgress();
                showAlertDialog("Unregister email");

            } else{

                closeProgress();
                showAlertDialog(String.valueOf(result_code));
            }

        } catch (JSONException e) {
            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }

    }

    private void updateToken(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATE_TOKEN;

        Log.d("Toke_URL==>", url);

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpdateToken(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_ID, String.valueOf(Commons.g_user.get_id()));
                    params.put(ReqConst.PARAM_TOKEN, Constants.TOKEN);

                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseUpdateToken(String json) {

        closeProgress();

        try {
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code ==  ReqConst.CODE_SUCCESS){

                gotoMain();

            } else {

                closeProgress();
                showAlertDialog(String.valueOf(result_code));
            }
        } catch (JSONException e) {

            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

    public void gotoMain(){

        startActivity(new Intent(SignInActivity.this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    public void gotoSignIn(View view){

        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    public void gotoSignUp(View view){

        startActivity(new Intent(this, SignUpActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    public void loginWithFaceBook() {

        callbackManager = CallbackManager.Factory.create();

        // set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                // Facebook Email address
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Log.v("LoginActivity Response ", response.toString());

                        try {

                            Name = object.getString("name");
                            Name.replace(" ", "");
                            Id=object.getString("id");
                            Firstname = object.getString("first_name");
                            Lastname = object.getString("last_name");
                            Gender = object.getString("gender");

                            //Password= object.getString("password");
                            FEmail = object.getString("email");
                            Image_url = "http://graph.facebook.com/(Id)/picture?type=large";
                            Image_url = URLEncoder.encode(Image_url);

                            Log.d("Email = ", " " + FEmail);
                            Log.d("Name======",Name);
                            Log.d("firstName======",Firstname);
                            Log.d("lastName======",Lastname);
                            Log.d("Gender======",Gender);
                            Log.d("id======",Id);

                            photourl = "http://graph.facebook.com/"+Id+"/picture?type=large";

                            SocialLogin(Name, Firstname, Lastname, FEmail, photourl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
    }


    //==================================Face book Login End======================================

    public void SocialLogin( final String username, final String firstname, final String lastname, final String useremail, final String photo_url){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_FACEBOOK ;
        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                //Log.d("response=====", String.valueOf(response));

                //parseEditResponse(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        closeProgress();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    String password = "123456";
                    //params.put("id",String.valueOf(Commons.g_user.get_idx()));

                    params.put("username",username);
                    params.put("firstname",firstname);
                    params.put("lastname",lastname);
                    params.put("useremail",  useremail);
                    params.put("photo_url", photo_url);
                    params.put("password", password);


                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REGISTER_CODE:

                if (resultCode == RESULT_OK) {

                    // load saved user
                    String email_phone = Preference.getInstance().getValue(this,
                            PrefConst.PREFKEY_USERNAME, "");
                    String userpwd = Preference.getInstance().getValue(this,
                            PrefConst.PREFKEY_USERPWD, "");

                    edt_email_phone.setText(email_phone);
                    edt_password.setText(userpwd);

                    processLogin();
                }
                break;
        }
    }

    private boolean checkValid(){

        if (edt_email_phone.getText().toString().length() == 0){

            showAlertDialog("Please input your email or phone number");
            return false;

        } else if (edt_password.getText().toString().length() == 0){
            showAlertDialog("Please input your password");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_signin:
                if (checkValid())
                processLogin();
                break;

            case R.id.txv_facebook_sign:
                loginWithFaceBook();
                break;
        }

    }
}
