package com.q2a.activitys;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.Utils.BitmapUtils;
import com.q2a.Utils.MultiPartRequest;
import com.q2a.Utils.RadiusImageView;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.UserEntity;
import com.q2a.multispinnerfilter.KeyPairBoolData;
import com.q2a.multispinnerfilter.MultiSpinnerSearch;
import com.q2a.multispinnerfilter.SpinnerListener;
import com.q2a.preference.PrefConst;
import com.q2a.preference.Preference;

import net.igenius.customcheckbox.CustomCheckBox;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubEditProfileActivity extends CommonActivity implements View.OnClickListener {

    private static final String TAG = "Subscriber Edit Profile Activity";

    ImageView imv_back;
    RadiusImageView imv_photo_sub_edt;

    EditText edt_firstName, edt_lastName, edt_email, edt_phoneNumber, edt_about_me;
    CustomCheckBox check_payment;
    TextView txv_save_profile, txv_subscriber, txv_payment;

    private Uri _imageCaptureUri;
    String _photoPath = "";

    ArrayList<String> subject = new ArrayList<String>();
    String _subjects = "", _description = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_edit_profile);

        checkAllPermission();
        loadLayout();
    }

    private void loadLayout() {

        final List<String> list = Arrays.asList(getResources().getStringArray(R.array.subject_array));
        final List<KeyPairBoolData> listArray1 = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId(i + 1);
            h.setName(list.get(i));
            h.setSelected(false);
            listArray1.add(h);
        }


        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProfile();
            }
        });

        imv_photo_sub_edt = (RadiusImageView) findViewById(R.id.imv_photo_sub_edt);
        imv_photo_sub_edt.setOnClickListener(this);

        edt_firstName = (EditText)findViewById(R.id.edt_firstName);
        edt_lastName = (EditText)findViewById(R.id.edt_lastName);
        edt_email = (EditText)findViewById(R.id.edt_email);
        edt_phoneNumber = (EditText)findViewById(R.id.edt_phone_number);
        edt_about_me = (EditText)findViewById(R.id.edt_about_me) ;

        txv_save_profile = (TextView)findViewById(R.id.txv_save_profile);
        txv_save_profile.setOnClickListener(this);

        txv_subscriber = (TextView)findViewById(R.id.txv_subscriber);
        txv_payment = (TextView)findViewById(R.id.txv_payment);

        check_payment = (CustomCheckBox)findViewById(R.id.check_payment);
        check_payment.setOnCheckedChangeListener(new CustomCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomCheckBox checkBox, boolean isChecked) {

                if (check_payment.isChecked()){

                    txv_payment.setTextColor(getResources().getColor(R.color.verify_color));
                    Commons.g_user.set_verify(true);
                    txv_payment.setText("Verified");

                } else {
                    Commons.g_user.set_verify(false);
                    Log.d("CustomCheckBox", String.valueOf(isChecked));
                    txv_payment.setTextColor(getResources().getColor(R.color.gray));
                    txv_payment.setText("Please payment verify");

                }
            }
        });

        MultiSpinnerSearch searchMultiSpinnerLimit = (MultiSpinnerSearch) findViewById(R.id.searchMultiSpinnerLimit);

        searchMultiSpinnerLimit.setItems(listArray1, -1, new SpinnerListener() {

            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {

                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
                        subject.add(items.get(i).getName());
                    }
                }

                //generate the subject string:
                _subjects=subject.get(0);
                for (int j = 1; j < subject.size(); j++){
                    _subjects+=":"+subject.get(j);
                }
            }
        });

        searchMultiSpinnerLimit.setLimit(3, new MultiSpinnerSearch.LimitExceedListener() {
            @Override
            public void onLimitListener(KeyPairBoolData data) {
                Toast.makeText(getApplicationContext(),
                        "You can't select the subject anymore ", Toast.LENGTH_LONG).show();
            }
        });

        showProfile();

    }

    private void showProfile(){

        if (Commons.g_user.get_photoUrl().length() > 0)
            Glide.with(this).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.bg_non_profile).into(imv_photo_sub_edt);

        edt_firstName.setText(Commons.g_user.get_first_name());
        edt_lastName.setText(Commons.g_user.get_last_name());
        edt_email.setText(Commons.g_user.get_email());
        edt_email.setEnabled(false);
        edt_phoneNumber.setText(String.valueOf(Commons.g_user.get_phone_number()));

        edt_about_me.setText(Commons.g_user.get_description());
        _description = edt_about_me.getText().toString().trim();

        if (Commons.g_user.get_subscriber() != 1){
            txv_subscriber.setText("I'm a student.");
        } else txv_subscriber.setText("I'm a subscriber.");

        if (Commons.g_user.is_verify()){

            txv_payment.setTextColor(getResources().getColor(R.color.verify_color));
            txv_payment.setText("Verified");
            check_payment.setChecked(true);

        } else {
            txv_payment.setText("Please payment verify");
            txv_payment.setTextColor(getResources().getColor(R.color.gray));
            check_payment.setChecked(false);

        }
    }

    public void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery","Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeGallery();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imv_photo_sub_edt.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void upLoadImage(){

       try {

           showProgress();
           File file = new File(_photoPath);

           Map<String, String> params = new HashMap<>();
           params.put(ReqConst.PARAM_ID, String.valueOf(Commons.g_user.get_id()));

           String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOAD_AVATAR;

           MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {
               @Override
               public void onErrorResponse(VolleyError error) {
                   if (isFinishing()) {

                       closeProgress();
                       showAlertDialog(getString(R.string.photo_upload_fail));
                   }
               }
           }, new Response.Listener<String>() {
               @Override
               public void onResponse(String response) {
                   parseUpLoadAvatar(response);
               }
           }, file, ReqConst.PARAM_FILE, params);

           reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
           Q2AApplication.getInstance().addToRequestQueue(reqMultiPart, url);

       } catch (Exception e){

           e.printStackTrace();
           closeProgress();
           showAlertDialog(getString(R.string.photo_upload_fail));
       }
    }
    private void parseUpLoadAvatar(String json){

        closeProgress();
        try {
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                Commons.g_user.set_photoUrl(response.getString(ReqConst.RES_AVATAR));

                upDateProfile();
            }

        } catch (JSONException e) {
            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void upDateProfile(){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATE_PROFILE;

        Log.d("URL===>", url);

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpDateProfile(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_ID, String.valueOf(Commons.g_user.get_id()));
                    params.put(ReqConst.PARAM_FIRST_NAME, edt_firstName.getText().toString());
                    params.put(ReqConst.PARAM_LAST_NAME, edt_lastName.getText().toString());
                    if (edt_about_me.getText().toString().length() > 0)
                        params.put(ReqConst.PARAM_DESCRIPTION, edt_about_me.getText().toString());
                    else params.put(ReqConst.PARAM_DESCRIPTION, "");
                    params.put(ReqConst.PARAM_PHONE, edt_phoneNumber.getText().toString());

                    if (_subjects.length() > 0) params.put(ReqConst.PARAM_SUBJECTS, _subjects);

                    else {

                        String subjects = Commons.g_user.get_subjects().get(0);
                        for (int j = 1; j < Commons.g_user.get_subjects().size(); j++){
                            subjects +=":"+Commons.g_user.get_subjects().get(j);
                        }
                        params.put(ReqConst.PARAM_SUBJECTS, subjects);
                    }


                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseUpDateProfile(String json) {

        try {

            Log.d("Response_profile==>", json);
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);
            if (result_code == ReqConst.CODE_SUCCESS){

                String _name = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERNAME, "");
                if (_name.equals(edt_phoneNumber.getText().toString()))
                    Preference.getInstance().put(this,
                            PrefConst.PREFKEY_USERNAME, edt_phoneNumber.getText().toString().trim());

                else Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERNAME, edt_email.getText().toString().trim());

                JSONObject user_info = response.getJSONObject(ReqConst.RES_USER_INFO);

                Commons.g_user.set_first_name(user_info.getString(ReqConst.RES_FIRST_NAME));
                Commons.g_user.set_last_name(user_info.getString(ReqConst.RES_LAST_NAME));
                Commons.g_user.set_phone_number(user_info.getString(ReqConst.RES_PHONE));
                Commons.g_user.set_description(user_info.getString(ReqConst.RES_DESCRIPTION));
                ArrayList<String> _subjects = new ArrayList<String>(Arrays.asList(user_info.getString(ReqConst.RES_SUBJECTS).split(":")));
                Commons.g_user.set_subjects(_subjects);

                startActivity(new Intent(this , MainActivity.class));
                Constants.EDIT_PROFILE = 1;

                finish();
            }

        } catch (JSONException e) {

            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

    public void saveSubProfile(){

        if (_photoPath.toString().length() > 0) upLoadImage();

        else upDateProfile();
    }

    private void gotoProfile(){

        Intent intent = new Intent(SubEditProfileActivity.this, MainActivity.class);
        startActivity(intent);
        Constants.EDIT_PROFILE = 1;
        overridePendingTransition(0,0);
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_photo_sub_edt:
                selectPhoto();
                break;

            case R.id.txv_save_profile:
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txv_save_profile.getWindowToken(),0);

                saveSubProfile();
                break;
        }

    }

    public static final int MY_PEQUEST_CODE = 104 ;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.CAMERA};

    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    //==================== CARMERA Permission========================================
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

        }
    }

    @Override
    public void onBackPressed() {
        gotoProfile();
    }
}
