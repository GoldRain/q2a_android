package com.q2a.activitys;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.Utils.BitmapUtils;
import com.q2a.Utils.CustomMultipartRequest;
import com.q2a.Utils.MediaRealPathUtil;
import com.q2a.Utils.MultiPartRequest;
import com.q2a.adapter.TimelineImageEditAdapter;
import com.q2a.adapter.TimelineVideoEditAdapter;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.QuestionEntity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.q2a.commons.Constants.PICK_FROM_VIDEO_GALLERY;

public class PostQuestionActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    RelativeLayout ui_rytProductCategory, ui_rytProductImages, ui_rytProductVideos;
    TextView txv_subject, txv_submit;
    EditText edt_title, edt_description;
    RecyclerView ui_revProductImages;

    RecyclerView recycler_video;
    TimelineVideoEditAdapter _adapter_video;
    TimelineImageEditAdapter _adapter;
    public static final int MAX_IMAGES = 6;

    ArrayList<String> _imagePaths = new ArrayList<>();
    ArrayList<String> _videoPaths = new ArrayList<>();
    ArrayList<String> _videoThumbPaths = new ArrayList<>();
    ArrayList<String> _videoCoplexPaths = new ArrayList<>();

    QuestionEntity _question = new QuestionEntity();

    ArrayList<String> _categorys = new ArrayList<>();

    private Uri _imageCaptureUri;
    String _photoPath = "";

    boolean _isSaving = false;

    String _thumPath, videoPath = "", _thumbPath = "";
    int _receiver_id = 0;
    int _question_id = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_question);

        if (Constants.FROM_NOTIFICATION == 200)
            _question = (QuestionEntity)getIntent().getSerializableExtra(Constants.KEY_QUESTION);

        else if (Constants.FROM_SUBSCRIBER == 100)
            _receiver_id =  getIntent().getIntExtra(Constants.KEY_RECEIVER_ID, 0);

        Log.d("====>Constant==>", String.valueOf(Constants.FROM_NOTIFICATION) + "  sub" + String.valueOf(Constants.FROM_SUBSCRIBER));
        Log.d("=====Id==>", String.valueOf(_receiver_id));

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMain();
            }
        });

        ui_rytProductCategory = (RelativeLayout)findViewById(R.id.ryt_product_category);
        ui_rytProductCategory.setOnClickListener(this);

        ui_rytProductImages = (RelativeLayout)findViewById(R.id.ryt_product_images);
        ui_rytProductImages.setOnClickListener(this);

        ui_rytProductVideos = (RelativeLayout)findViewById(R.id.ryt_product_videos);
        ui_rytProductVideos.setOnClickListener(this);

        edt_title = (EditText)findViewById(R.id.edt_product_title);
        edt_description = (EditText)findViewById(R.id.edt_product_description);
        txv_subject = (TextView)findViewById(R.id.txv_product_category);

        txv_submit = (TextView)findViewById(R.id.txv_submit);
        txv_submit.setOnClickListener(this);

        //imagesAdapter
        ui_revProductImages = (RecyclerView) findViewById(R.id.recycler_image);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL, false);
        ui_revProductImages.setLayoutManager(layoutManager);

        _adapter = new TimelineImageEditAdapter(this);
        ui_revProductImages.setAdapter(_adapter);

        //VIdeoAdapter
        recycler_video = (RecyclerView)findViewById(R.id.recycler_video);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL, false);
        recycler_video.setLayoutManager(layoutManager1);
        _adapter_video = new TimelineVideoEditAdapter(this);
        recycler_video.setAdapter(_adapter_video);

        deleteAllTempImages();

        cameraSetting();

        if (Constants.FROM_NOTIFICATION == 200) showQuestion(_question);

    }

    private void showQuestion(QuestionEntity question){

        edt_title.setText(question.get_title());
        edt_description.setText(question.get_description());
        if (question.get_subjects().size() > 0)
            txv_subject.setText(question.get_subjects().get(0));

        ArrayList<String> imageUrls = new ArrayList<>();
        for (int i = 0; i < question.get_images_url().size(); i++){

            imageUrls.add(question.get_images_url().get(i).get_image_url());
        }
        addImages(imageUrls);

        addVideo(question.get_video_url(), question.get_thumb_url());
    }

    private void cameraSetting() {

        MediaRecorder mediaRecorder =  new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
        mediaRecorder.setVideoSize(640, 480);
        mediaRecorder.setVideoFrameRate(10);
    }

    public void addImage(String path) {

        _imagePaths.add(path);
        _adapter.setDatas(_imagePaths);
        _adapter.notifyDataSetChanged();

    }

    private void addVideo(String videoPath, String thumbPath){

        _videoPaths.add(videoPath);
        _videoThumbPaths.add(thumbPath);

        _adapter_video.setDatas(_videoThumbPaths, _videoPaths);

    }

    public void addImages(ArrayList<String> paths) {

        _imagePaths.addAll(paths);

        _adapter.setDatas(_imagePaths);

    }

    public void removeImage(String path) {

        _imagePaths.remove(path);
        _adapter.setDatas(_imagePaths);

    }

    public void removeVideo(String path, String path1) {

        _videoThumbPaths.remove(path);
        _videoPaths.remove(path1);
        _adapter.setDatas(_imagePaths);

    }

    public void onSelectPhoto(){

        if (_imagePaths.size() >= MAX_IMAGES) {

            showAlertDialog("You can only select up to 6 photos.");
            return ;

        }

        final String[] items = { "Take photo","Choose from Gallary","Cancel"};
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);


        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0){
                    doTakePhoto();

                }else if (item == 1){

                    doTakeGallery();

                }else return;
            }

        });

        android.support.v7.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void onSelectVideo(){

        if (_videoPaths.size() == 1){

            showAlertDialog("You can only select up one video");
            return;
        }

        final String[] items = {"Take video","Take Video from Gallery","Cancel"};
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);


        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {
                    doTakeVideo();

                } else if (item == 1) {
                    takeVideoGallary();

                } else {
                    return;
                }
            }
        });

        android.support.v7.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void doTakeVideo(){

        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,  BitmapUtils.getVideoThumbFolderPath());
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
        startActivityForResult(intent,Constants.PICK_FROM_VIDEO);
    }

    public void takeVideoGallary()
    {
        Intent intent;
        if(android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
        {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        }
        else
        {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.INTERNAL_CONTENT_URI);
        }

        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(intent,PICK_FROM_VIDEO_GALLERY);
    }

    private void getThumbnail(String _videoPath){

        File saveFile = BitmapUtils.getOutputMediaFile(this);

        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(_videoPath, MediaStore.Images.Thumbnails.MINI_KIND);

        _thumPath = saveFile.getAbsolutePath();

        _thumbPath = getThumPath(thumb);

        addVideo(_videoPath, _thumbPath);

        Log.d("Thumbnail========>", _thumbPath);

    }

    private String getThumPath(Bitmap thumb) {

        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/Temp";
        File dir = new File(file_path);
        if(!dir.exists())
            dir.mkdirs();

        long random = new Date().getTime();

        File file = new File(dir, "temp_ex" + random + ".png");

        try {

            FileOutputStream fOut = new FileOutputStream(file);

            thumb.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath()  + "photo_temp.jpg" ;
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(this, SelectImageActivity.class);
        intent.putExtra(Constants.KEY_COUNT, _imagePaths.size());
        startActivityForResult(intent, Constants.PICK_FROM_IMAGES);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode) {

            case PICK_FROM_VIDEO_GALLERY:

                if (resultCode == RESULT_OK) {

                    if (data.getData() != null) {

                        Uri w_uri = data.getData();

                        String w_strPath = MediaRealPathUtil.getPath(this, w_uri);

                        String filename = Commons.fileNameWithExtFromUrl(w_strPath);

                        if (w_strPath == null) {

                            showToast(getString(R.string.getvideo_fail));
                            return;
                        }

                        videoPath = w_strPath;
                        getThumbnail(w_strPath);

                    } else {
                        showToast("Failed to select video");
                    }
                }
                break;

            case Constants.PICK_FROM_VIDEO:

                if (resultCode == RESULT_OK) {

                    Uri w_uri = data.getData();

                    String w_strPath = MediaRealPathUtil.getPath(this, w_uri);

                    String filename = Commons.fileNameWithExtFromUrl(w_strPath);

                    if (w_strPath == null) {
                        showToast(getString(R.string.getvideo_fail));
                        return;
                    }

                    videoPath = w_strPath;
                    getThumbnail(w_strPath);

                }
                break;


            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK) {

                    try {

                        //File outFile = new File(_photoPath);
                        File outFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(outFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        ExifInterface ei = new ExifInterface(outFile.getAbsolutePath());
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                        Bitmap returnedBitmap = bitmap;

                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 90);
                                // Free up the memory
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 180);
                                // Free up the memory
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 270);
                                // Free up the memory
                                bitmap.recycle();
                                bitmap = null;
                                break;

                            default:
                                returnedBitmap = bitmap;
                        }

                        Bitmap w_bmpSizeLimited = Bitmap.createScaledBitmap(returnedBitmap, Constants.PROFILE_IMAGE_SIZE, Constants.PROFILE_IMAGE_SIZE, true);

                        BitmapUtils.saveOutput(outFile, w_bmpSizeLimited);

                        _photoPath = outFile.getAbsolutePath();

                        addImage(_photoPath);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }

            case Constants.PICK_FROM_IMAGES:

                if (resultCode == RESULT_OK){
                    ArrayList<String> paths = data.getStringArrayListExtra(Constants.KEY_IMAGES);
                    addImages(paths);
                }

                break;
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK) {

                    _imageCaptureUri = data.getData();
                }


            case Constants.PICK_FROM_CAMERA:
            {

                if (resultCode == RESULT_OK) {
                    try {

                        _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                        //beginGrop(_imageCaptureUri ) ;

                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... params) {

                                //We send the message here.
                                //You should also check if the suername is valid here.
                                try {

                                    selectPhoto();

                                } catch (Exception e) {

                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);

                                addImage(_photoPath);                                       // add photo  :

                            }

                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
        }
    }

    private void selectPhoto() {

        try {
            Bitmap returnedBitmap = BitmapUtils.loadOrientationAdjustedBitmap(_photoPath);

            Log.d("=======", _photoPath) ;

            File outFile = getOutputMediaFile();

            BitmapUtils.saveOutput(outFile , returnedBitmap);
            returnedBitmap.recycle();
            returnedBitmap = null;

            _photoPath = outFile.getAbsolutePath();
        }    catch (Exception e) {

            e.printStackTrace();
        }
    }

    private void beginCrop(Uri source) {

        File outFile = getOutputMediaFile();
        Uri destination = Uri.fromFile(outFile);
        _photoPath = outFile.getAbsolutePath();
        //Crop.of(source, destination).asSquare().start(this);
    }


    public void deleteAllTempImages() {

        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory() + "/android/data/"
                        + this.getPackageName() + "/myprofile");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return;
            }
        } else {
            String[] children = mediaStorageDir.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(mediaStorageDir, children[i]).delete();
            }
        }
    }

    public File getOutputMediaFile() {

        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory() + "/android/data/"
                        + this.getPackageName() + "/myprofile");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        long random = new Date().getTime();

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "temp" + random + ".png");

        return mediaFile;
    }

    private boolean checkValue(){

        if(edt_title.getText().length() == 0){
            showAlertDialog("Please enter title");
            return false;

        } else if(edt_description.getText().length() == 0){
            showAlertDialog("Please enter description");
            return false;

        }else if(txv_subject.getText().length() == 0){
            showAlertDialog("Please select subject.");
            return false;
        }

        return true;
    }


    public void selectSubject() {

        _categorys.clear();

        ArrayList<String> _categories = new ArrayList<>();

        _categorys.add("math");_categorys.add("physics");_categorys.add("history"); _categorys.add("Biology"); _categorys.add("English");_categorys.add("Chemistry");

        final String[] categoryArray = new String[ _categorys.size()];

        for(int i = 0; i < _categorys.size(); i++){

            //CategoryEntity categoryEntity = _categorys.get(i);

            categoryArray[i]=_categorys.get(i);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setItems(categoryArray, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                txv_subject.setText(categoryArray[item]);
                //selected_category_id = _categorys.get(item).getCategory_id();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void gotoMain(){

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void submitRequest() {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txv_submit.getWindowToken(), 0);

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SUBMIT_REQUEST;
        Log.d("Submit_req_URL =>", url);

        showProgress();

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSubmitRequest(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> parmas = new HashMap<>();

                try {
                    parmas.put(ReqConst.PARAM_TITLE, edt_title.getText().toString());
                    parmas.put(ReqConst.PARAM_SENDER_ID, String.valueOf(Commons.g_user.get_id()));

                    //case from noti or home
                    if (Constants.FROM_NOTIFICATION == 200){
                        _receiver_id = _question.get_other_id();
                        parmas.put(ReqConst.PARAM_RECEIVER_ID, String.valueOf(_receiver_id));

                    } else if (Constants.FROM_SUBSCRIBER == 100)
                        parmas.put(ReqConst.PARAM_RECEIVER_ID, String.valueOf(_receiver_id));

                    Log.d("====Receiver Id==>", String.valueOf(_receiver_id));

                    parmas.put(ReqConst.PARAM_DESCRIPTION, edt_description.getText().toString());
                    parmas.put(ReqConst.PARAM_SUBJECTS, txv_subject.getText().toString());

                } catch (Exception e){}

                return parmas;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseSubmitRequest(String json) {

        closeProgress();
        Log.d("question_respon==>", json);

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _question_id = response.getInt(ReqConst.RES_ID);

                if (_imagePaths.size() > 0) uploadImages();
                else if (_videoPaths.size() > 0 && _videoThumbPaths.size() > 0) uploadThumbnail();
                else gotoMain();

            }
        } catch (JSONException e) {
            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

    public void uploadImages() {

        if (_isSaving) {
            return;
        }

        _isSaving = true;

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SAVE_MULTI_FILES;

        Map<String, String> mHeaderPart= new HashMap<>();
        mHeaderPart.put("Content-type", "multipart/form-data;");

        //File part
        Map<String, File> mFilePartData= new HashMap<>();

        for (int i = 0; i < _imagePaths.size(); i++) {

            String path = _imagePaths.get(i);
            if (path.length() > 0)

                mFilePartData.put("file[" + i + "]", new File(path));
        }

        //String part
        Map<String, String> mStringPart= new HashMap<>();

        mStringPart.put(ReqConst.PARAM_ID, String.valueOf(_question_id));

        CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, this, url, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject jsonObject) {

                parseUploadResponse(jsonObject.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showToast("Images upload failed");
                closeProgress();
                _isSaving = false;
            }
        }, mFilePartData, mStringPart, mHeaderPart);

        mCustomRequest.setRetryPolicy(new DefaultRetryPolicy(600000,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Q2AApplication.getInstance().addToRequestQueue(mCustomRequest, url);

    }

    public void parseUploadResponse(String json){

        closeProgress();

        Log.d("===Parse==UPImages==>", json);

        _isSaving = false;

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                Toast.makeText(_context, "Images upload Success!", Toast.LENGTH_SHORT).show();

                //gotoMain();
                if (_videoPaths.size() > 0 && _videoThumbPaths.size() > 0) uploadThumbnail();
                else gotoMain();

            } else {

                closeProgress();
                showAlertDialog(getString(R.string.photo_upload_fail));
            }

        } catch (JSONException e){
            showAlertDialog(getString(R.string.photo_upload_fail));
            e.printStackTrace();
        }
    }


    public void uploadThumbnail(){

        File file_video = new File(videoPath);

        if (!file_video.exists()) return;

        if (file_video.length() > Constants.LIMIT_FILE) {
            showAlertDialog(getString(R.string.file_overflow));
            return;
        }
        showProgress();

        try {

            File file = new File(_thumbPath);

            Map<String, String> params = new HashMap<String, String>();

            params.put(ReqConst.PARAM_ID, String.valueOf(_question_id));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOAD_THUMB_NAIL;
            Log.d("===URL_ThUM==>",url);

            MultiPartRequest reqMultiPartThnmb = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    closeProgress();
                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null) {
                        Log.e("Volley", "Error. HTTP Status Code:"+networkResponse.statusCode);
                    }

                    if (error instanceof TimeoutError) {
                        Log.e("Volley", "TimeoutError");
                    }else if(error instanceof NoConnectionError){
                        Log.e("Volley", "NoConnectionError");
                    } else if (error instanceof AuthFailureError) {
                        Log.e("Volley", "AuthFailureError");
                    } else if (error instanceof ServerError) {
                        Log.e("Volley", "ServerError");
                    } else if (error instanceof NetworkError) {
                        Log.e("Volley", "NetworkError");
                    } else if (error instanceof ParseError) {
                        Log.e("Volley", "ParseError");
                    }
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    ParseUploadThumbNail(json);
                }
            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPartThnmb.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            Q2AApplication.getInstance().addToRequestQueue(reqMultiPartThnmb, url);

        } catch (Exception e) {

            e.printStackTrace();
            closeProgress();
            showToast(getString(R.string.photo_upload_fail));
        }
    }
    /*/////////////////////////////*/

    public void ParseUploadThumbNail(String json){

        Log.d("===Parse==UPThum==>", json);

        closeProgress();

        try{
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                uploadVideo();
            }
            else {

                gotoMain();
            }
        }catch (JSONException e){

            e.printStackTrace();

        }

    }

    public void uploadVideo(){

        showProgress();

        try {

            File file = new File(videoPath);

            Map<String, String> params = new HashMap<String, String>();

            params.put(ReqConst.PARAM_ID, String.valueOf(_question_id));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOAD_VIDEO;

            Log.d("===upLoadVideo==>", url);

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    closeProgress();
                    showToast("Upload Video Failed.!");
                    gotoMain();

                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    parseVideoResponse(json);
                }

            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            Q2AApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            closeProgress();
        }
    }

    public void parseVideoResponse(String json){

        closeProgress();

        try{

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){
                gotoMain();
                //finish();

            } else {
                gotoMain();
            }

        }catch (JSONException e){
            showAlertDialog("Video upload failed");
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.ryt_product_category:
                selectSubject();
                break;

            case R.id.ryt_product_images:
                onSelectPhoto();
                break;

            case R.id.ryt_product_videos:
                onSelectVideo();
                break;

            case R.id.txv_submit:
                if (checkValue())
                submitRequest();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        gotoMain();
    }
}
