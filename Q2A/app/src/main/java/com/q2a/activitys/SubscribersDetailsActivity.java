package com.q2a.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.q2a.R;
import com.q2a.Utils.RadiusImageView;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.UserEntity;

public class SubscribersDetailsActivity extends CommonActivity implements View.OnClickListener{

    TextView txv_firstName, txv_lastName, txv_email, txv_phoneNumber, txv_verify, txv_subscriber,txv_name, txv_description;
    TextView txv_subject1,txv_subject2, txv_subject3;
    ImageView imv_back,imv_photo,imv_verify;

    UserEntity _subscriber = new UserEntity();
    TextView txv_request;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribers_details);

        Constants.FROM_SUBSCRIBER = 0;

        _subscriber = (UserEntity)getIntent().getSerializableExtra("subscriber");
        loadLayout();
    }

    private void loadLayout() {

        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imv_photo = (RadiusImageView)findViewById(R.id.imv_photo);

        txv_firstName = (TextView)findViewById(R.id.txv_firstName);
        txv_lastName = (TextView)findViewById(R.id.txv_lastName);
        txv_name = (TextView)findViewById(R.id.txv_name);
        txv_email = (TextView)findViewById(R.id.txv_email);
        txv_phoneNumber = (TextView)findViewById(R.id.txv_phoneNumber);
        txv_verify = (TextView)findViewById(R.id.txv_verify);

        txv_subject1 = (TextView)findViewById(R.id.txv_subject1);
        txv_subject2 = (TextView)findViewById(R.id.txv_subject2);
        txv_subject3 = (TextView)findViewById(R.id.txv_subject3);

        txv_subscriber = (TextView)findViewById(R.id.txv_subscriber);
        txv_description = (TextView)findViewById(R.id.txv_description);

        txv_request = (TextView) findViewById(R.id.txv_request);
        txv_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("Subsriber++>", String.valueOf(_subscriber.get_id()));
                Constants.FROM_SUBSCRIBER = 100;
                Intent intent = new Intent(SubscribersDetailsActivity.this, PostQuestionActivity.class);
                intent.putExtra(Constants.KEY_RECEIVER_ID, _subscriber.get_id());
                overridePendingTransition(0,0);
                startActivity(intent);
                finish();

            }
        });

        imv_verify = (ImageView)findViewById(R.id.imv_verify);

        showProfile();
    }

    private void showProfile(){

        Glide.with(this).load(_subscriber.get_photoUrl()).placeholder(R.drawable.bg_non_profile).into(imv_photo);
        txv_name.setText(_subscriber.get_full_name());
        txv_firstName.setText(_subscriber.get_first_name());
        txv_lastName.setText(_subscriber.get_last_name());
        txv_email.setText(_subscriber.get_email());
        txv_phoneNumber.setText(String.valueOf(_subscriber.get_phone_number()));
        txv_description.setText(_subscriber.get_description());

        if (_subscriber.get_subscriber() != 1){
            txv_subscriber.setText("I'm a student.");
        } else txv_subscriber.setText("I'm a subscriber.");

        if (_subscriber.is_verify()){

            txv_verify.setTextColor(getResources().getColor(R.color.verify_color));
            imv_verify.setImageResource(R.drawable.ic_check_verify);
            txv_verify.setText("verified");

        } else {

            txv_verify.setTextColor(getResources().getColor(R.color.gray));
            imv_verify.setImageResource(R.drawable.ic_unverified);
            txv_verify.setText("Not verified");
        }

        if (_subscriber.get_subjects().size() == 3){

            txv_subject1.setText(_subscriber.get_subjects().get(0));
            txv_subject2.setText(_subscriber.get_subjects().get(1));
            txv_subject3.setText(_subscriber.get_subjects().get(2));

        } else if (_subscriber.get_subjects().size() == 2){

            txv_subject1.setText(_subscriber.get_subjects().get(0));
            txv_subject2.setText(_subscriber.get_subjects().get(1));
            txv_subject3.setVisibility(View.GONE);

        } else {
            txv_subject1.setText(_subscriber.get_subjects().get(0));
            txv_subject2.setVisibility(View.GONE);
            txv_subject3.setVisibility(View.GONE);
        }

    }
    @Override
    public void onClick(View v) {

    }
}
