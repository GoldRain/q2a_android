package com.q2a.activitys;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.q2a.R;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;

public class IntroActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        Constants.INTRO_ACT = this;
        gotoLogin();
    }

    private void gotoLogin() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(IntroActivity.this, SignInActivity.class));
                finish();
            }
        },500);
    }
}
