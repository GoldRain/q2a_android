package com.q2a.base;

import android.support.v4.app.Fragment;

/**
 * Created by HGS on 12/11/2015.
 */

public abstract class BaseFragment  extends Fragment {

    public BaseActivity _context;

    public void showProgress(){

        _context.showProgress();
    }

    public void CloseProgress(){

        _context.closeProgress();
    }

    public void showToast(String strMsg){

        _context.showToast(strMsg);
    }

    public void showAlert(String strMsg){

        _context.showAlertDialog(strMsg);
    }
}
