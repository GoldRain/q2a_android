package com.q2a;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.q2a.activitys.IntroActivity;
import com.q2a.base.CommonActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;

/**
 * Created by HugeRain on 4/7/2017.
 */

public class RestartActivity extends CommonActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!Commons.g_isAppRunning){

            String room = getIntent().getStringExtra(Constants.KEY_ROOM);

            Intent goIntro = new Intent(this, IntroActivity.class);

            if (room != null)
                goIntro.putExtra(Constants.KEY_ROOM, room);

            startActivity(goIntro);
        }

        finish();

    }


    @Override
    protected void onDestroy(){

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){

        return true;
    }

    @Override
    public void onClick(View v) {

    }
}
