package com.q2a.commons;

import java.util.logging.Handler;

public class ReqConst {

    public static final String SERVER_ADDR = "http://54.68.180.131";
    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;

    public static Handler g_handler = null;
    public static String g_appVersion = "1.0";
    public static int g_badgCount = 0;

    //public static PatronEntity g_newUser = null;
    //public static PatronEntity g_user = null;


    //public static final String SERVER_ADDR = "http://192.168.1.50:2150";


    public static final String SERVER_URL = SERVER_ADDR + "/index.php/api_qa/";

    //Request value
    public static final String REQ_SINGUP = "signup";
    public static final String REQ_LOGIN = "login";
    public static final String REQ_FACEBOOK = "loginwithfacebook";
    public static final String REQ_UPLOAD_THUMB_NAIL = "uploadThumbnail";
    public static final String REQ_UPLOAD_VIDEO = "uploadVideo";
    public static final String REQ_SAVE_MULTI_FILES = "saveMulitiFiles";
    public static final String REQ_ALL_REQUEST = "getAllRequests";
    public static final String REQ_UPLOAD_AVATAR = "uploadAvatar";
    public static final String REQ_UPDATE_PROFILE = "updateProfile";
    public static final String RES_ALLNOTIFICATION = "getAllNotifications";
    public static final String REQ_ACCEPT_REQUEST = "acceptRequest";
    public static final String REQ_DELETE_REQUEST = "deleteRequest";
    public static final String REQ_REJECT_REQUEST = "rejectRequest";
    public static final String REQ_REMOVE_NOTIFICATION = "removeNotification";
    public static final String REQ_RESUBMIT_REQUEST = "reSubmitRequest";
    public static final String REQ_GETS_SUBSCRIBERS = "getSubscribers";
    public static final String REQ_UPDATE_TOKEN = "updateToken";
    public static final String REQ_NOTIFICATION = "notification";
    public static final String REQ_SUBMIT_REQUEST = "submitRequest";

    //Request Params

    public static final String PARAM_ID = "id";
    public static final String RES_NOTI_ID = "noti_id";
    public static final String PARAM_FILE = "file";
    public static final String PARAM_FIRST_NAME = "firstname";
    public static final String PARAM_LAST_NAME = "lastname";
    public static final String PARAM_DESCRIPTION = "description";
    public static final String PARAM_TITLE ="title";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_PHONE = "phone";
    public static final String PARAM_SUBSCRIBER = "subscriber";
    public static final String PARAM_SUBJECTS = "subject";
    public static final String PARAM_TOKEN = "token";
    public static final String PARAM_SENDER_ID = "sender";
    public static final String PARAM_RECEIVER_ID = "receiver";

    //response value

    /*LOGIN*/
    public static final String RES_USER_INFO = "userInfo";
    public static final String RES_DATE = "date";
    public static final String RES_USER_ID = "id";
    public static final String RES_FIRST_NAME = "firstname";
    public static final String RES_LAST_NAME = "lastname";
    public static final String RES_EMAIL = "email";
    public static final String RES_PASSWORD = "password";
    public static final String RES_PHONE = "phone";
    public static final String RES_PHOTO_URL = "avatar";
    public static final String RES_SUBJECTS = "subjects";
    public static final String RES_SUBJECT = "subject";
    public static final String RES_DESCRIPTION = "description";
    public static final String RES_SUBSCRIBER = "subscriber";
    public static final String RES_IMAGES = "images";
    public static final String RES_IMAGE = "image";
    public static final String RES_NOTI_CNT = "notiCount";

    //home data
    public static final String RES_ALL_REQUEST = "all_request";
    //uploadProfile
    public static final String RES_AVATAR = "avatar";
    //getAllNoti
    public static final String RES_ALL_NOTIFICATION = "all_notification";
    //getSubscribers
    public static final String RES_SUBSCRIBERS = "subscribers";

    public static final String RES_ID = "id";
    public static final String RES_TITLE = "title";
    public static final String RES_THUMBNAIL = "thumbnail";
    public static final String RES_VIDEO = "video";
    public static final String RES_SUBMIT_DATE = "submit_date";
    public static final String RES_STATUS = "status";
    public static final String RES_PAYMENT = "payment";
    public static final String RES_PERMISSION = "permission";


    public static final String RES_CODE = "result_code";

/*
    101 : Exist email or phone number
    102 : Unregistered email or phone number
    105: Failed to upload file*/

    public static final int CODE_SUCCESS = 200;
    public static final int CODE_EXIST_EMAIL = 101;
    public static final int CODE_UNREGISTER = 102;
    public static final int CODE_FAILED = 105;



}
