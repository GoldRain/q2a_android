package com.q2a.commons;

import com.q2a.activitys.IntroActivity;
import com.q2a.activitys.MainActivity;
import com.q2a.fragments.HomeFragment;
import com.q2a.fragments.NotificationFragment;
import com.q2a.fragments.subscriber.Sub_HomeFragment;
import com.q2a.fragments.subscriber.Sub_NotiFragment;
import com.q2a.model.QuestionEntity;

import java.util.ArrayList;

/**
 * Created by HugeRain on 2/26/2017.
 */

public class Constants {

    public static final int VOLLEY_TIME_OUT = 60000;

    public static String TOKEN = "";

    public static final int LIMIT_FILE = 25 * 1024 * 1024;

    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final int RECENT_MESSAGE_COUNT = 20;
    public static final int MAX_IMAGE_COUNT = 6;

    public static final String KEY_COUNT = "count";
    public static final int SPLASH_TIME = 2000;
    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int PICK_FROM_IMAGES = 116;
    public static final int PICK_FROM_VIDEO = 104;
    public static final int PICK_FROM_VIDEO_GALLERY = 106;
    public static final String KEY_IMAGES = "images";

    public static final String KEY_ROOM = "room";
    public static final String KEY_LOGOUT = "logout";

    public static final int ANDROID = 0;

    public static  String qrstring = " ";

    public static int FILTER_STATUS = 0; //0: all: 1: request: 2:accepted:3:rejected

    //public static boolean SUBSCRIBER = false;/* false:student, true:subscriber */

    public static int NOTI_COUNTER = 0;

    public static MainActivity mainActivity = null;
    public static Sub_HomeFragment SUB_HOME_FRAGMENT = null;
    public static Sub_NotiFragment SUB_NOTI_FRAGMENT = null;
    public static HomeFragment STUDENT_HOME_FRAGMENT = null;
    public static NotificationFragment STUDENT_NOTI_FRAGMENT = null;

    public static IntroActivity INTRO_ACT = null;

    public static ArrayList<QuestionEntity> QUESTIONS = new ArrayList<>();

    public static final String KEY_VIDEOPATH = "video_path";
    public static int TAKE_VIDEO = 0;
    public static String VIDEO_PATH = "";

    public static final String KEY_IMAGEPATH = "image_path";
    public static final String KEY_POSITION = "position";
    public static final String KEY_COMMENT = "comment";
    public static int EDIT_PROFILE = 0;  // EDIT_PROFILE=1:
    public static final String KEY_RECEIVER_ID = "receiver_id";
    public static final String KEY_QUESTION = "question";
    public static final String KEY_SUBSCRIBER = "subscriber";
    public static int FROM_SUBSCRIBER = 0;
    public static int FROM_NOTIFICATION = 0;

    public static ArrayList<QuestionEntity> ALL_QUESTION = new ArrayList<>();
}
