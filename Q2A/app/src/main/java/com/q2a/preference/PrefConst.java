package com.q2a.preference;

/**
 * Created by ToSuccess on 11/29/2016.
 */

public class PrefConst {

    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_XMPPID = "XMPPID";
    public static final String PREFKEY_USERPWD = "password";
    public static final String PREFKEY_USERNAME = "username";

    public static final String PREFKEY_PHOTURL= "photourl";
    public static final String PREFKEY_ID= "id";

    public static final String PREFKEY_PUSH = "push_setting";
    public static final String PREFKEY_ALLOWFRIEND = "allow_friend";

    public static final String PREFKEY_LASTLOGINID = "lastlogin_id";

    public static final String PREFKEY_TOKEN = "tokenid";

}

