package com.q2a.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by HugeRain on 4/9/2017.
 */

public class QuestionEntity implements Serializable {

    int _id = 0;
    int _noti_id = 0;
    int _status = 0; // 0 : request 1: accepted : 2: rejected:
    int _paid = 0;

    String _title = "";
    String _description = "";
    //
    int _other_id = 0;
    String _other_imageUrl = "";
    String _other_phone = "";

    String _submit_time = "";
    String _video_url = "";
    String _thumb_url = "";
    String _submit_date = "";
    ArrayList<ImagesEntity> _images_url = new ArrayList<>();
    ArrayList<String> _subjects = new ArrayList<>();  //subjects


    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int get_noti_id() {
        return _noti_id;
    }

    public void set_noti_id(int _noti_id) {
        this._noti_id = _noti_id;
    }

    public int get_status() {
        return _status;
    }

    public void set_status(int _status) {
        this._status = _status;
    }

    public int get_paid() {
        return _paid;
    }

    public void set_paid(int _paid) {
        this._paid = _paid;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_video_url() {
        return _video_url;
    }

    public void set_video_url(String _video_url) {
        this._video_url = _video_url;
    }

    public String get_thumb_url() {
        return _thumb_url;
    }

    public void set_thumb_url(String _thumb_url) {
        this._thumb_url = _thumb_url;
    }

    public ArrayList<ImagesEntity> get_images_url() {
        return _images_url;
    }

    public void set_images_url(ArrayList<ImagesEntity> _images_url) {
        this._images_url = _images_url;
    }

    public ArrayList<String> get_subjects() {
        return _subjects;
    }

    public void set_subjects(ArrayList<String> _subjects) {
        this._subjects = _subjects;
    }

    public String get_submit_date() {
        return _submit_date;
    }

    public void set_submit_date(String _submit_date) {
        this._submit_date = _submit_date;
    }

    public String get_other_imageUrl() {
        return _other_imageUrl;
    }

    public void set_other_imageUrl(String _other_imageUrl) {
        this._other_imageUrl = _other_imageUrl;
    }

    public String get_other_phone() {
        return _other_phone;
    }

    public void set_other_phone(String _other_phone) {
        this._other_phone = _other_phone;
    }

    public int get_other_id() {
        return _other_id;
    }

    public void set_other_id(int _other_id) {
        this._other_id = _other_id;
    }

    public String get_submit_time() {
        return _submit_time;
    }

    public void set_submit_time(String _submit_time) {
        this._submit_time = _submit_time;
    }
}
