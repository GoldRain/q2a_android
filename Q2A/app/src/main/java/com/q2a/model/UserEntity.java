package com.q2a.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by HugeRain on 4/7/2017.
 */

public class UserEntity implements Serializable {

    int _id = 0;
    int _noti_cnt = 0;
    String _first_name = "";
    String _last_name = "";
    String _description = "";
    String _email = "";
    String _phone_number = "";
    String _photoUrl = "";
    /*boolean _student = false;*/ //false: student: true:subscriber:
    int _subscriber = 0; // 1: subscriber ; 0: student:
    int _payment = 0;
    boolean _verify = false;

    ArrayList<String> _subjects = new ArrayList<>();

    ArrayList<QuestionEntity> _content = new ArrayList<>();

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int get_noti_cnt() {
        return _noti_cnt;
    }

    public void set_noti_cnt(int _noti_cnt) {
        this._noti_cnt = _noti_cnt;
    }

    public String get_first_name() {
        return _first_name;
    }

    public void set_first_name(String _first_name) {
        this._first_name = _first_name;
    }

    public String get_last_name() {
        return _last_name;
    }

    public void set_last_name(String _last_name) {
        this._last_name = _last_name;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_full_name(){

       return  _first_name + " " + _last_name;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public String get_phone_number() {
        return _phone_number;
    }

    public void set_phone_number(String _phone_number) {
        this._phone_number = _phone_number;
    }

    public int get_subscriber() {
        return _subscriber;
    }

    public void set_subscriber(int _subscriber) {
        this._subscriber = _subscriber;
    }

    public String get_photoUrl() {
        return _photoUrl;
    }

    public void set_photoUrl(String _photoUrl) {
        this._photoUrl = _photoUrl;
    }

    public boolean is_verify() {
        return _verify;
    }

    public void set_verify(boolean _verify) {
        this._verify = _verify;
    }

    public ArrayList<QuestionEntity> get_content() {
        return _content;
    }

    public void set_content(ArrayList<QuestionEntity> _content) {
        this._content = _content;
    }

    public ArrayList<String> get_subjects() {
        return _subjects;
    }

    public void set_subjects(ArrayList<String> _subjects) {
        this._subjects = _subjects;
    }

    public int get_payment() {
        return _payment;
    }

    public void set_payment(int _payment) {
        this._payment = _payment;
    }
}
