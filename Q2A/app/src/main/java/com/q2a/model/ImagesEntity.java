package com.q2a.model;

import java.io.Serializable;

/**
 * Created by HugeRain on 5/14/2017.
 */

public class ImagesEntity implements Serializable {

    int _id = 0;
    String _image_url = "";

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_image_url() {
        return _image_url;
    }

    public void set_image_url(String _image_url) {
        this._image_url = _image_url;
    }
}
