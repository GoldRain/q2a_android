package com.q2a.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.activitys.MainActivity;
import com.q2a.adapter.StudentHomeListViewAdapter;
import com.q2a.adapter.Sub_HomeListViewAdapter;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.ImagesEntity;
import com.q2a.model.QuestionEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class HomeFragment extends Fragment {

    MainActivity _activity;
    View view;

    ListView lst_home_questions;
    StudentHomeListViewAdapter _adapter;

    ArrayList<QuestionEntity> _questions = new ArrayList<>();

    ArrayList<QuestionEntity> _all_questions =  new ArrayList<>();

    ArrayList<QuestionEntity> _request_array = new ArrayList<>();
    ArrayList<QuestionEntity> _reject_array = new ArrayList<>();
    ArrayList<QuestionEntity> _accepted_array = new ArrayList<>();


    public HomeFragment(MainActivity activity) {
        // Required empty public constructor
        this._activity = activity;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        Constants.FROM_NOTIFICATION = 0; // from_notification initial goto Post QuestionActivity
        Constants.STUDENT_HOME_FRAGMENT = this; //noti fragment detect
        loadLayout();
        return view;
    }

    public void getAllHome(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ALL_REQUEST;

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseGetHomeData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>(); {

                    params.put("id", String.valueOf(Commons.g_user.get_id()));
                    //params.put("id", String.valueOf(1));
                }
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseGetHomeData(String json) {

        Log.d("resoonse==>", json);

        try {
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _activity.closeProgress();
                _all_questions = new ArrayList<>();
                _questions = new ArrayList<>();
                JSONArray all_request = response.getJSONArray(ReqConst.RES_ALL_REQUEST);
                for (int i = 0; i < all_request.length(); i++){

                    QuestionEntity questionEntity = new QuestionEntity();
                    JSONObject json_request = (JSONObject)all_request.get(i);

                    questionEntity.set_id(json_request.getInt(ReqConst.RES_ID));
                    questionEntity.set_title(json_request.getString(ReqConst.RES_TITLE));
                    questionEntity.set_description(json_request.getString(ReqConst.RES_DESCRIPTION));

                    ArrayList<String> _subjects = new ArrayList<String>(Arrays.asList(json_request.getString(ReqConst.RES_SUBJECT).split(",")));
                    questionEntity.set_subjects(_subjects);

                    Log.d("_subject---", json_request.getString(ReqConst.RES_SUBJECT));

                    ArrayList<ImagesEntity> _images = new ArrayList<>();
                    JSONArray json_images = json_request.getJSONArray(ReqConst.RES_IMAGES);
                    for (int j = 0; j < json_images.length(); j++){

                        JSONObject _image = (JSONObject)json_images.get(j);
                        ImagesEntity imagesEntity = new ImagesEntity();

                        imagesEntity.set_id(_image.getInt(ReqConst.RES_ID));
                        imagesEntity.set_image_url(_image.getString(ReqConst.RES_IMAGE));

                        _images.add(imagesEntity);
                    }

                    questionEntity.set_images_url(_images);

                    questionEntity.set_thumb_url(json_request.getString(ReqConst.RES_THUMBNAIL));
                    questionEntity.set_video_url(json_request.getString(ReqConst.RES_VIDEO));

                    Log.d("===>video==>", questionEntity.get_video_url());

                    //show_other part
                    JSONObject json_from = json_request.getJSONObject("to");
                    questionEntity.set_other_id(json_from.getInt(ReqConst.RES_ID));
                    questionEntity.set_other_imageUrl(json_from.getString("avatar"));
                    questionEntity.set_other_phone(json_from.getString(ReqConst.RES_PHONE));

                    //if (json_request.getInt(ReqConst.RES_STATUS)==2) continue;
                    questionEntity.set_status(json_request.getInt(ReqConst.RES_STATUS));

                    _all_questions.add(questionEntity);

                    // 0 : request 1: accepted : 2: rejected:
                    if(questionEntity.get_status() == 0) _request_array.add(questionEntity);

                    else if(questionEntity.get_status() == 1) _accepted_array.add(questionEntity);

                    else if (questionEntity.get_status() == 2) _reject_array.add(questionEntity);

                }

                //0: all: 1: request: 2:accepted:3:rejected

                if (Constants.FILTER_STATUS == 1){

                    _questions.addAll(_request_array);

                } else if (Constants.FILTER_STATUS == 2){

                    _questions.addAll(_accepted_array);

                } else if (Constants.FILTER_STATUS == 3){

                    _questions.addAll(_reject_array);

                } else if (Constants.FILTER_STATUS ==0){

                    _questions.addAll(_all_questions);
                }

                _adapter = new StudentHomeListViewAdapter(_activity, _questions);
                lst_home_questions.setAdapter(_adapter);
                _adapter.notifyDataSetChanged();

            } else {

                _activity.closeProgress();
                _activity.showAlertDialog(String.valueOf(result_code));
            }

        } catch (JSONException e) {

            _activity.closeProgress();
            _activity.showAlertDialog(_activity.getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void loadLayout() {

        lst_home_questions = (ListView) view.findViewById(R.id.lst_home_student);
        _adapter = new StudentHomeListViewAdapter(_activity, _questions);
        lst_home_questions.setAdapter(_adapter);

        _adapter.notifyDataSetChanged();

        getAllHome();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Constants.STUDENT_HOME_FRAGMENT = null;
    }
}
