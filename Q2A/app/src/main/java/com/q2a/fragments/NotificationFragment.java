package com.q2a.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.activitys.MainActivity;
import com.q2a.adapter.NotiListViewAdapter;
import com.q2a.adapter.Sub_NotiListViewAdapter;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.ImagesEntity;
import com.q2a.model.QuestionEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class NotificationFragment extends Fragment {


    MainActivity _activity;
    View view;
    ArrayList<QuestionEntity> _notifications = new ArrayList<>();

    ListView lst_noti;
    NotiListViewAdapter _adapter;

    public NotificationFragment(MainActivity activity) {
        // Required empty public constructor
        _activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_notification, container, false);
        Constants.STUDENT_NOTI_FRAGMENT = this;
        loadLayout();
        return view;
    }

    public void getAllNotification1(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.RES_ALLNOTIFICATION;

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseGetHomeData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>(); {

                    params.put(ReqConst.PARAM_ID, String.valueOf(Commons.g_user.get_id()));
                    //params.put("id", String.valueOf(1));
                }
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseGetHomeData(String json) {

        try {
            JSONObject response = new JSONObject(json);
            Constants.NOTI_COUNTER = response.getInt(ReqConst.RES_NOTI_CNT);

            int result_code = response.getInt(ReqConst.RES_CODE);
            if (result_code == ReqConst.CODE_SUCCESS){

                _activity.closeProgress();

                _notifications = new ArrayList<>();
                Log.d("===notiSize==>", String.valueOf(_notifications.size()) + "  " + String.valueOf(result_code));
                JSONArray all_request = response.getJSONArray(ReqConst.RES_ALL_NOTIFICATION);

                for (int i = 0; i < all_request.length(); i++){

                    QuestionEntity questionEntity = new QuestionEntity();
                    JSONObject json_notification = (JSONObject)all_request.get(i);

                    questionEntity.set_id(json_notification.getInt(ReqConst.RES_ID));
                    questionEntity.set_noti_id(json_notification.getInt(ReqConst.RES_NOTI_ID));
                    questionEntity.set_title(json_notification.getString(ReqConst.RES_TITLE));
                    questionEntity.set_description(json_notification.getString(ReqConst.RES_DESCRIPTION));
                    questionEntity.set_status(json_notification.getInt(ReqConst.RES_STATUS));

                    ArrayList<String> _subjects = new ArrayList<String>(Arrays.asList(json_notification.getString("subject").split(":")));
                    questionEntity.set_subjects(_subjects);

                    Log.d("_subject---", json_notification.getString("subject"));

                    ArrayList<ImagesEntity> _images = new ArrayList<>();
                    JSONArray json_images = json_notification.getJSONArray(ReqConst.RES_IMAGES);
                    for (int j = 0; j < json_images.length(); j++){

                        JSONObject _image = (JSONObject)json_images.get(j);
                        ImagesEntity imagesEntity = new ImagesEntity();

                        imagesEntity.set_id(_image.getInt(ReqConst.RES_ID));
                        imagesEntity.set_image_url(_image.getString(ReqConst.RES_IMAGE));

                        _images.add(imagesEntity);
                    }

                    questionEntity.set_images_url(_images);

                    questionEntity.set_thumb_url(json_notification.getString(ReqConst.RES_THUMBNAIL));
                    questionEntity.set_video_url(json_notification.getString(ReqConst.RES_VIDEO));

                    //show_other part
                    JSONObject json_from = json_notification.getJSONObject("to");
                    questionEntity.set_other_id(json_from.getInt(ReqConst.RES_ID));
                    questionEntity.set_other_imageUrl(json_from.getString("avatar"));
                    questionEntity.set_other_phone(json_from.getString(ReqConst.RES_PHONE));

                    _notifications.add(questionEntity);
                }

                _adapter = new NotiListViewAdapter(_activity, _notifications);
                lst_noti.setAdapter(_adapter);
                _adapter.notifyDataSetChanged();

            } else {

                _activity.closeProgress();
                _activity.showAlertDialog(String.valueOf(result_code));
            }

        } catch (JSONException e) {

            _activity.closeProgress();
            _activity.showAlertDialog(_activity.getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void loadLayout() {

        if (Constants.QUESTIONS.size() != 0){

            _notifications = Constants.QUESTIONS;
        }

        lst_noti = (ListView)view.findViewById(R.id.lst_noti);
   /*     _adapter = new NotiListViewAdapter(_activity, _notifications);
        lst_noti.setAdapter(_adapter);

        _adapter.notifyDataSetChanged();*/

        getAllNotification1();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Constants.STUDENT_NOTI_FRAGMENT = null;
    }
}
