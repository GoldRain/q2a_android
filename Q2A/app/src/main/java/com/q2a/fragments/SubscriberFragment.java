package com.q2a.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.activitys.MainActivity;
import com.q2a.adapter.SubscriberListAdapter;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.UserEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class SubscriberFragment extends Fragment {

    MainActivity _activity;
    View view;

    ListView lst_subscriber;
    SubscriberListAdapter _adapter_sub ;
    ArrayList<UserEntity> _subscribers = new ArrayList<>();

    public SubscriberFragment(MainActivity activity) {
        // Required empty public constructor
        _activity = activity;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_subscriber, container, false);

        loadLayout();
        return view;
    }

    private void getSubscribers() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETS_SUBSCRIBERS;

        _activity.showProgress();

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseGetSubscribers(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseGetSubscribers(String json) {

        _activity.closeProgress();

        try {
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray subscribers = response.getJSONArray(ReqConst.RES_SUBSCRIBERS);
                for (int i = 0; i < subscribers.length(); i++){

                    UserEntity subscriber = new UserEntity();
                    JSONObject json_subscriber = (JSONObject)subscribers.get(i);

                    subscriber.set_id(json_subscriber.getInt(ReqConst.RES_ID));
                    subscriber.set_first_name(json_subscriber.getString(ReqConst.RES_FIRST_NAME));
                    subscriber.set_last_name(json_subscriber.getString(ReqConst.RES_LAST_NAME));
                    subscriber.set_photoUrl(json_subscriber.getString(ReqConst.RES_AVATAR));
                    subscriber.set_email(json_subscriber.getString(ReqConst.RES_EMAIL));
                    subscriber.set_phone_number(json_subscriber.getString(ReqConst.RES_PHONE));
                    subscriber.set_description(json_subscriber.getString(ReqConst.RES_DESCRIPTION));
                    subscriber.set_subscriber(json_subscriber.getInt(ReqConst.RES_SUBSCRIBER));
                    subscriber.set_payment(json_subscriber.getInt(ReqConst.RES_PAYMENT));

                    //get subjects with ArrayLis
                    ArrayList<String> _subjects = new ArrayList<String>(Arrays.asList(json_subscriber.getString(ReqConst.RES_SUBJECTS).split(":")));
                    subscriber.set_subjects(_subjects);

                    _subscribers.add(subscriber);
                }

                _adapter_sub = new SubscriberListAdapter(_activity, _subscribers);
                lst_subscriber.setAdapter(_adapter_sub);

                _adapter_sub.notifyDataSetChanged();

            }

        } catch (JSONException e) {
            _activity.closeProgress();
            _activity.showAlertDialog(_activity.getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void loadLayout() {

        lst_subscriber = (ListView)view.findViewById(R.id.lst_subscriber);

        getSubscribers();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}
