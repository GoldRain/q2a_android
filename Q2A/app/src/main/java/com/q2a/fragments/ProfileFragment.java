package com.q2a.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.q2a.R;
import com.q2a.Utils.RadiusImageView;
import com.q2a.activitys.MainActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;

import java.util.ArrayList;


public class ProfileFragment extends Fragment implements View.OnClickListener{

    MainActivity _activity;
    View view;

    TextView txv_firstName, txv_lastName, txv_email, txv_phoneNumber, txv_verify, txv_student, txv_edit_profile; //txv_subject;
    ImageView imv_photo,imv_verify;

    public ProfileFragment(MainActivity activity) {
        // Required empty public constructor
        _activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        imv_photo = (RadiusImageView)view.findViewById(R.id.imv_photo);

        txv_firstName = (TextView)view.findViewById(R.id.txv_firstName);
        txv_lastName = (TextView)view.findViewById(R.id.txv_lastName);
        txv_email = (TextView)view.findViewById(R.id.txv_email);
        txv_phoneNumber = (TextView)view.findViewById(R.id.txv_phoneNumber);
        txv_verify = (TextView)view.findViewById(R.id.txv_verify);

        txv_student = (TextView)view.findViewById(R.id.txv_student);
        //txv_subject = (TextView)view.findViewById(R.id.txv_subject);
        imv_verify = (ImageView)view.findViewById(R.id.imv_verify);

        txv_edit_profile = (TextView) view.findViewById(R.id.txv_edit_profile);
        txv_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.gotoProfileEdit();
            }
        });

        showProfile();

    }

    private void showProfile(){

        if (Commons.g_user.get_photoUrl().length() > 0)
            Glide.with(_activity).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.bg_non_profile).into(imv_photo);
        txv_firstName.setText(Commons.g_user.get_first_name());
        txv_lastName.setText(Commons.g_user.get_last_name());
        txv_email.setText(Commons.g_user.get_email());
        txv_phoneNumber.setText(String.valueOf(Commons.g_user.get_phone_number()));

      /*  if (Commons.g_user.get_subjects().size() > 0){

            txv_subject.setVisibility(View.VISIBLE);
            txv_subject.setText(Commons.g_user.get_subjects().get(0));

        } else {
            txv_subject.setVisibility(View.GONE);
        }*/

        if (Commons.g_user.get_subscriber() == 0){
            txv_student.setText("I'm a student.");
        } else txv_student.setText("I'm a subscriber.");

        if (Commons.g_user.is_verify()){

            txv_verify.setTextColor(getResources().getColor(R.color.verify_color));
            imv_verify.setImageResource(R.drawable.ic_check_verify);
            txv_verify.setText("Verified");

        } else {

            txv_verify.setTextColor(getResources().getColor(R.color.gray));
            imv_verify.setImageResource(R.drawable.ic_unverified);
            txv_verify.setText("Not verified");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

    @Override
    public void onClick(View v) {

    }
}
