package com.q2a.adapter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.q2a.R;
import com.q2a.activitys.PostQuestionActivity;
import com.q2a.activitys.VideoPreviewActivity;
import com.q2a.commons.Constants;

import java.util.ArrayList;


public class TimelineVideoEditAdapter extends RecyclerView.Adapter<TimelineVideoEditAdapter.VideoHolder>{

    private ArrayList<String> _imageUrls = new ArrayList<>();
    private ArrayList<String> _videos = new ArrayList<>();
    PostQuestionActivity _context;

    public TimelineVideoEditAdapter(PostQuestionActivity context) {

        this._context = context;
    }

    @Override
    public TimelineVideoEditAdapter.VideoHolder onCreateViewHolder(ViewGroup viewGroup, final int position) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_takephoto_video_edit, viewGroup, false);

        VideoHolder viewHolder = new VideoHolder(view);
        return viewHolder;
    }

    public void setDatas(ArrayList<String> images, ArrayList<String> videos) {
        _imageUrls = images;
        _videos = videos;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(VideoHolder viewHolder, int i) {

        String imageUrl = _imageUrls.get(i);
        Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);

        viewHolder.imvPhoto.setImageBitmap(bitmap);

        final int position = i;

        viewHolder.imvPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(_context, VideoPreviewActivity.class);
                intent.putExtra(Constants.KEY_VIDEOPATH, _videos.get(position));
                _context.startActivity(intent);


            }
        });

        viewHolder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _context.removeVideo(_imageUrls.get(position), _videos.get(position));
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != _imageUrls ? _imageUrls.size() : 0);
    }


    public class VideoHolder extends RecyclerView.ViewHolder {

        ImageView imvPhoto;
        ImageView imvDelete;
        ImageView imvVideoPlay;

        public VideoHolder(View view) {

            super(view);
            imvPhoto = (ImageView) view.findViewById(R.id.imv_image);
            imvDelete = (ImageView) view.findViewById(R.id.imv_delete);
            imvVideoPlay = (ImageView)view.findViewById(R.id.imv_videoplay);
        }
    }
}
