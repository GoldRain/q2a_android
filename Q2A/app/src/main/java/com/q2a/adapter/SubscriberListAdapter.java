package com.q2a.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.q2a.R;
import com.q2a.Utils.RadiusImageView;
import com.q2a.activitys.MainActivity;
import com.q2a.model.UserEntity;

import java.util.ArrayList;

/**
 * Created by HugeRain on 4/9/2017.
 */

public class SubscriberListAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<UserEntity> _allSubscriber = new ArrayList<>();

    public SubscriberListAdapter (MainActivity activity, ArrayList<UserEntity> subscribers){

        _activity = activity;
        _allSubscriber = subscribers;
    }

    @Override
    public int getCount() {
        return _allSubscriber.size();
    }

    @Override
    public Object getItem(int position) {
        return _allSubscriber.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SubscriberHolder holder;

        if (convertView == null){

            holder = new SubscriberHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_subscriber, parent, false);

            holder.imv_photo = (RadiusImageView)convertView.findViewById(R.id.imv_photo_sub);
            holder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            holder.txv_description = (TextView)convertView.findViewById(R.id.txv_description);
            holder.txv_subject1 = (TextView)convertView.findViewById(R.id.txv_subject1);
            holder.txv_subject2 = (TextView)convertView.findViewById(R.id.txv_subject2);
            holder.txv_subject3 = (TextView)convertView.findViewById(R.id.txv_subject3);

            convertView.setTag(holder);

        }else {

            holder = (SubscriberHolder) convertView.getTag();
        }

        final UserEntity subscriber = (UserEntity)_allSubscriber.get(position);

        if (subscriber.get_photoUrl().length() > 0)
            Glide.with(_activity).load(subscriber.get_photoUrl()).placeholder(R.drawable.bg_non_profile).into(holder.imv_photo);
        holder.txv_name.setText(subscriber.get_full_name());
        holder.txv_description.setText(subscriber.get_description());

        Log.d("subject_size==>", String.valueOf(subscriber.get_subjects().size()));

        if (subscriber.get_subjects().size() == 1){

            holder.txv_subject1.setText(subscriber.get_subjects().get(0));
            holder.txv_subject2.setVisibility(View.GONE);
            holder.txv_subject3.setVisibility(View.GONE);
            holder.txv_subject1.setVisibility(View.VISIBLE);


        } else if (subscriber.get_subjects().size() == 2){

            holder.txv_subject1.setText(subscriber.get_subjects().get(0));
            holder.txv_subject2.setText(subscriber.get_subjects().get(1));
            holder.txv_subject3.setVisibility(View.GONE);
            holder.txv_subject1.setVisibility(View.VISIBLE);
            holder.txv_subject2.setVisibility(View.VISIBLE);

        } else {

            holder.txv_subject1.setText(subscriber.get_subjects().get(0));
            holder.txv_subject2.setText(subscriber.get_subjects().get(1));
            holder.txv_subject3.setText(subscriber.get_subjects().get(2));
            holder.txv_subject1.setVisibility(View.VISIBLE);
            holder.txv_subject2.setVisibility(View.VISIBLE);
            holder.txv_subject3.setVisibility(View.VISIBLE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.gotoSubscriberDetails(subscriber);
            }
        });

        return convertView;
    }

    public class SubscriberHolder{

        RadiusImageView imv_photo;
        TextView txv_name;
        TextView txv_description;
        TextView txv_subject1;
        TextView txv_subject2;
        TextView txv_subject3;

    }
}
