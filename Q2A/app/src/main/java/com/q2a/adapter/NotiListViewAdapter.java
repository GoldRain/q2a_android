package com.q2a.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.Utils.RadiusImageView;
import com.q2a.activitys.MainActivity;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.QuestionEntity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HugeRain on 4/10/2017.
 */

public class NotiListViewAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<QuestionEntity> _allNotis = new ArrayList<>();

    public NotiListViewAdapter (MainActivity activity, ArrayList<QuestionEntity> notis){

        _activity = activity;
        _allNotis = notis;

    }

    @Override
    public int getCount() {
        return _allNotis.size();
    }

    @Override
    public Object getItem(int position) {
        return _allNotis.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        NotiHolder holder;

        if(convertView ==  null){

            holder = new NotiHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_noti, parent, false);

            holder.imv_photo = (RadiusImageView)convertView.findViewById(R.id.imv_photo_noti);
            holder.txv_name = (TextView)convertView.findViewById(R.id.txv_name_noti);
            holder.txv_description = (TextView)convertView.findViewById(R.id.txv_description);
            holder.txv_subject1 = (TextView)convertView.findViewById(R.id.txv_subject1);
            holder.txv_subject2 = (TextView)convertView.findViewById(R.id.txv_subject2);
            holder.txv_subject3 = (TextView)convertView.findViewById(R.id.txv_subject3);
            holder.txv_noti_status = (TextView)convertView.findViewById(R.id.txv_noti_status);

            holder.txv_subject1.setVisibility(View.GONE);
            holder.txv_subject2.setVisibility(View.GONE);
            holder.txv_subject3.setVisibility(View.GONE);
            convertView.setTag(holder);

        } else {

            holder = (NotiHolder)convertView.getTag();
        }

        final QuestionEntity noti = _allNotis.get(position);

        if (noti.get_other_imageUrl().length() > 0)
            Glide.with(_activity).load(noti.get_other_imageUrl()).placeholder(R.drawable.bg_non_profile).into(holder.imv_photo);
        holder.txv_name.setText(noti.get_title());
        holder.txv_description.setText(noti.get_description());

        Log.d("=====subject", noti.get_subjects().get(0));

        if (noti.get_subjects().size() == 1){

            if (noti.get_subjects().get(0).length() == 0)
            holder.txv_subject1.setText(noti.get_subjects().get(0));
            holder.txv_subject2.setVisibility(View.GONE);
            holder.txv_subject3.setVisibility(View.GONE);

            holder.txv_subject1.setVisibility(View.VISIBLE);

        } else if (noti.get_subjects().size() == 2){

            holder.txv_subject1.setText(noti.get_subjects().get(0));
            holder.txv_subject2.setText(noti.get_subjects().get(1));
            holder.txv_subject3.setVisibility(View.GONE);

            holder.txv_subject1.setVisibility(View.VISIBLE);
            holder.txv_subject2.setVisibility(View.VISIBLE);

        } else if (noti.get_subjects().size() == 3){

            holder.txv_subject1.setText(noti.get_subjects().get(0));
            holder.txv_subject2.setText(noti.get_subjects().get(1));
            holder.txv_subject3.setText(noti.get_subjects().get(2));

            holder.txv_subject1.setVisibility(View.VISIBLE);
            holder.txv_subject2.setVisibility(View.VISIBLE);
            holder.txv_subject3.setVisibility(View.VISIBLE);

        } else if (noti.get_subjects().size() == 0){

            holder.txv_subject1.setVisibility(View.GONE);
            holder.txv_subject2.setVisibility(View.GONE);
            holder.txv_subject3.setVisibility(View.GONE);
        }

        /*1: accepted : 2: rejected:*/

        if (noti.get_status() == 1){

            holder.txv_noti_status.setText("Accepted");
            holder.txv_noti_status.setTextColor(_activity.getResources().getColor(R.color.white));
            holder.txv_noti_status.setBackgroundResource(R.drawable.accept_round_fill);

        } else if (noti.get_status() == 2){

            holder.txv_noti_status.setText("Rejected");
            holder.txv_noti_status.setTextColor(_activity.getResources().getColor(R.color.black));
            holder.txv_noti_status.setBackgroundResource(R.drawable.red_fill);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = ReqConst.SERVER_URL + ReqConst.REQ_REMOVE_NOTIFICATION;

                _activity.showProgress();
                StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String json) {

                        _activity.closeProgress();

                        Log.d("delete===response",json);

                        try {
                            JSONObject response = new JSONObject(json);

                            int result_code = response.getInt(ReqConst.RES_CODE);
                            if (result_code == ReqConst.CODE_SUCCESS){

                                _activity.gotoShowDetails(noti);
                                revomeNoti(noti);

                                if(Constants.mainActivity != null){

                                    Constants.mainActivity.decreasebage();
                                }

                                notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            _activity.closeProgress();
                            _activity.showAlertDialog(_activity.getString(R.string.error));
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        _activity.closeProgress();
                        _activity.showAlertDialog(_activity.getString(R.string.error));
                    }
                }){

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        try {
                            params.put(ReqConst.PARAM_ID, String.valueOf(noti.get_noti_id()));
                        } catch (Exception e){}

                        return params;
                    }
                };

                request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                Q2AApplication.getInstance().addToRequestQueue(request, url);

            }
        });


        return convertView;
    }

    private void revomeNoti(QuestionEntity entity){

        _allNotis.remove(entity);
    }

    public class NotiHolder{

        RadiusImageView imv_photo;
        TextView txv_name;
        TextView txv_description;
        TextView txv_subject1;
        TextView txv_subject2;
        TextView txv_subject3;
        TextView txv_noti_status;
    }
}
