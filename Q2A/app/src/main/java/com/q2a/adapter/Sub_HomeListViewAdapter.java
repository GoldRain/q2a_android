package com.q2a.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.q2a.R;
import com.q2a.Utils.RadiusImageView;
import com.q2a.activitys.MainActivity;
import com.q2a.model.QuestionEntity;

import java.util.ArrayList;

import jp.shts.android.library.TriangleLabelView;

/**
 * Created by HugeRain on 4/16/2017.
 */

public class Sub_HomeListViewAdapter extends BaseAdapter {

    static final int TYPE_REQUESTED = 0;
    static final int TYPE_ACCEPTED = 1;

    MainActivity _activity;
    ArrayList<QuestionEntity> _questions = new ArrayList<>();

    public Sub_HomeListViewAdapter(MainActivity activity, ArrayList<QuestionEntity> questions){

        _activity = activity;
        _questions = questions;
    }

    @Override
    public int getCount() {
        return _questions.size();
    }

    @Override
    public Object getItem(int position) {
        return _questions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        // 0 : request 1: accepted :
        if (_questions.get(position).get_status() == 0){

            return TYPE_REQUESTED;

        } else  return TYPE_ACCEPTED;

    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        int type = getItemViewType(position);

        switch (type){

            case TYPE_REQUESTED: {

                RequestHolder holder;

                if (convertView == null) {

                    holder = new RequestHolder();

                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_request_home, parent, false);

                    holder.lyt_request = (LinearLayout)convertView.findViewById(R.id.lyt_request);
                    holder.imv_sub_photo = (RadiusImageView) convertView.findViewById(R.id.imv_photo_sub);
                    holder.txv_title = (TextView) convertView.findViewById(R.id.txv_title);
                    holder.txv_contents = (TextView) convertView.findViewById(R.id.txv_content);
                    holder.label_view = (TriangleLabelView) convertView.findViewById(R.id.label_view);

                    convertView.setTag(holder);

                } else {

                    holder = (RequestHolder) convertView.getTag();
                }

                final QuestionEntity question = (QuestionEntity) _questions.get(position);

                Glide.with(_activity).load(question.get_other_imageUrl()).placeholder(R.drawable.bg_non_profile).into(holder.imv_sub_photo);
                holder.txv_title.setText(question.get_title());
                holder.txv_contents.setText(question.get_description());

                holder.label_view.setPrimaryText("Requested");
                holder.label_view.setTriangleBackgroundColor(_activity.getResources().getColor(R.color.blue));

                holder.lyt_request.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        _activity.gotoShowDetails(question);
                    }
                });

            }

            break;

            case TYPE_ACCEPTED:{

                AcceptedHolder holder;

                if (convertView == null) {

                    holder = new AcceptedHolder();

                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_accept_home, parent, false);

                    holder.lyt_accepted = (LinearLayout)convertView.findViewById(R.id.lyt_accepted);
                    holder.imv_sub_photo = (RadiusImageView) convertView.findViewById(R.id.imv_photo_sub);
                    holder.txv_title = (TextView) convertView.findViewById(R.id.txv_title);
                    holder.txv_contents = (TextView) convertView.findViewById(R.id.txv_content);
                    holder.label_view = (TriangleLabelView) convertView.findViewById(R.id.label_view);
                    holder.rlt_call = (RelativeLayout) convertView.findViewById(R.id.rlt_call);

                    convertView.setTag(holder);

                } else {

                    holder = (AcceptedHolder) convertView.getTag();
                }

                final QuestionEntity question = (QuestionEntity) _questions.get(position);
                if (question.get_other_imageUrl().length() > 0)
                    Glide.with(_activity).load(question.get_other_imageUrl()).placeholder(R.drawable.bg_non_profile).into(holder.imv_sub_photo);
                holder.txv_title.setText(question.get_title());
                holder.txv_contents.setText(question.get_description());

                holder.label_view.setPrimaryText("Accepted");
                holder.label_view.setTriangleBackgroundColor(_activity.getResources().getColor(R.color.green));

                holder.rlt_call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData( Uri.parse("tel:" +  question.get_other_phone()));

                        if (ActivityCompat.checkSelfPermission(_activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        _activity.startActivity(callIntent);
                    }
                });

                holder.lyt_accepted.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        _activity.gotoShowDetails(question);
                    }
                });


            }

            break;
        }

        return convertView;
    }

    public class RequestHolder {

        LinearLayout lyt_request;
        RadiusImageView imv_sub_photo;
        TextView txv_title;
        TextView txv_contents;
        TriangleLabelView label_view;
    }

    public class AcceptedHolder {

        LinearLayout lyt_accepted;
        RadiusImageView imv_sub_photo;
        TextView txv_title;
        TextView txv_contents;
        RelativeLayout rlt_call;
        TriangleLabelView label_view;
    }
}
