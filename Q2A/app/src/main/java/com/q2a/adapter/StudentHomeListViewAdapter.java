package com.q2a.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.Utils.RadiusImageView;
import com.q2a.activitys.MainActivity;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.QuestionEntity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jp.shts.android.library.TriangleLabelView;

/**
 * Created by HugeRain on 4/9/2017.
 */

public class StudentHomeListViewAdapter extends BaseAdapter {

    static final int TYPE_REQUESTED = 0;
    static final int TYPE_ACCEPTED = 1;
    static final int TYPE_REJECTED = 2;

    MainActivity _activity;

    ArrayList<QuestionEntity> _questions = new ArrayList<>();

    public StudentHomeListViewAdapter(MainActivity activity, ArrayList<QuestionEntity> questions){

        _activity = activity;
        _questions =(questions);

    }

    @Override
    public int getCount() {
        return _questions.size();
    }

    @Override
    public Object getItem(int position) {
        return _questions.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        // 0 : request 1: accepted : 2: rejected:
        if (_questions.get(position).get_status() == 0){

            return TYPE_REQUESTED;

        } else if (_questions.get(position).get_status() == 1){
            return TYPE_ACCEPTED;

        } else return TYPE_REJECTED;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        switch (type){

            case TYPE_REQUESTED: {

                RequestHolder holder;

                if (convertView == null) {

                    holder = new RequestHolder();

                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_request_home, parent, false);

                    holder.lyt_request = (LinearLayout)convertView.findViewById(R.id.lyt_request);
                    holder.imv_sub_photo = (RadiusImageView) convertView.findViewById(R.id.imv_photo_sub);
                    holder.txv_title = (TextView) convertView.findViewById(R.id.txv_title);
                    holder.txv_contents = (TextView) convertView.findViewById(R.id.txv_content);
                    holder.label_view = (TriangleLabelView) convertView.findViewById(R.id.label_view);

                    convertView.setTag(holder);

                } else {

                    holder = (RequestHolder) convertView.getTag();
                }

                final QuestionEntity question = (QuestionEntity) _questions.get(position);

                Glide.with(_activity).load(question.get_other_imageUrl()).placeholder(R.drawable.bg_non_profile).into(holder.imv_sub_photo);
                holder.txv_title.setText(question.get_title());
                holder.txv_contents.setText(question.get_description());

                holder.label_view.setPrimaryText("Requested");
                holder.label_view.setTriangleBackgroundColor(_activity.getResources().getColor(R.color.blue));

                holder.lyt_request.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        _activity.gotoShowDetails(question);
                    }
                });

            }

            break;

            case TYPE_ACCEPTED:{

                AcceptedHolder holder;

                if (convertView == null) {

                    holder = new AcceptedHolder();

                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_accept_home, parent, false);

                    holder.lyt_accepted = (LinearLayout) convertView.findViewById(R.id.lyt_accepted);
                    holder.imv_sub_photo = (RadiusImageView) convertView.findViewById(R.id.imv_photo_sub);
                    holder.txv_title = (TextView) convertView.findViewById(R.id.txv_title);
                    holder.txv_contents = (TextView) convertView.findViewById(R.id.txv_content);
                    holder.label_view = (TriangleLabelView) convertView.findViewById(R.id.label_view);
                    holder.rlt_call = (RelativeLayout) convertView.findViewById(R.id.rlt_call);

                    convertView.setTag(holder);

                } else {

                    holder = (AcceptedHolder) convertView.getTag();
                }

                final QuestionEntity question = (QuestionEntity) _questions.get(position);

                if (question.get_other_imageUrl().length() > 0)
                    Glide.with(_activity).load(question.get_other_imageUrl()).placeholder(R.drawable.bg_non_profile).into(holder.imv_sub_photo);
                holder.txv_title.setText(question.get_title());
                holder.txv_contents.setText(question.get_description());

                holder.label_view.setPrimaryText("Accepted");
                holder.label_view.setTriangleBackgroundColor(_activity.getResources().getColor(R.color.green));

                holder.lyt_accepted.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        _activity.gotoShowDetails(question);
                    }
                });

                holder.rlt_call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData( Uri.parse("tel:" +  question.get_other_phone()));

                        if (ActivityCompat.checkSelfPermission(_activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        _activity.startActivity(callIntent);

                    }
                });

            }

            break;

            case TYPE_REJECTED: {

                RejectedHolder holder;

                if (convertView == null) {

                    holder = new RejectedHolder();

                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_rejected_home, parent, false);

                    holder.lyt_reject = (LinearLayout)convertView.findViewById(R.id.lyt_reject);
                    holder.imv_sub_photo = (RadiusImageView) convertView.findViewById(R.id.imv_photo_sub);
                    holder.txv_title = (TextView) convertView.findViewById(R.id.txv_title);
                    holder.txv_contents = (TextView) convertView.findViewById(R.id.txv_content);
                    holder.label_view = (TriangleLabelView) convertView.findViewById(R.id.label_view);

                    holder.rlt_delete = (RelativeLayout) convertView.findViewById(R.id.rlt_delete);
                    holder.rlt_edit = (RelativeLayout) convertView.findViewById(R.id.rlt_edit);
                    holder.rlt_resend = (RelativeLayout) convertView.findViewById(R.id.rlt_resend);

                    convertView.setTag(holder);

                } else {

                    holder = (RejectedHolder) convertView.getTag();
                }

                final QuestionEntity question = (QuestionEntity) _questions.get(position);

                Glide.with(_activity).load(question.get_other_imageUrl()).placeholder(R.drawable.bg_non_profile).into(holder.imv_sub_photo);
                holder.txv_title.setText(question.get_title());
                holder.txv_contents.setText(question.get_description());

                holder.label_view.setPrimaryText("Rejected");
                holder.label_view.setTriangleBackgroundColor(_activity.getResources().getColor(R.color.red));


                holder.rlt_edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        _activity.gotoPostQuestion(question);
                    }
                });

                holder.rlt_resend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            String url = ReqConst.SERVER_URL + ReqConst.REQ_RESUBMIT_REQUEST;

                            _activity.showProgress();
                            StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String json) {

                                    _activity.closeProgress();

                                    Log.d("resubmit===response",json);

                                    try {

                                        JSONObject response = new JSONObject(json);

                                        int result_code = response.getInt(ReqConst.RES_CODE);
                                        if (result_code == ReqConst.CODE_SUCCESS){

                                            _activity.gotoHomeFragment();
                                        }
                                    } catch (JSONException e) {
                                        _activity.closeProgress();
                                        _activity.showAlertDialog(_activity.getString(R.string.error));
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    _activity.closeProgress();
                                    _activity.showAlertDialog(_activity.getString(R.string.error));
                                }
                            }){

                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {

                                    Map<String, String> params = new HashMap<>();

                                    try {
                                        params.put(ReqConst.PARAM_ID, String.valueOf(question.get_id()));
                                        params.put(ReqConst.PARAM_TITLE, question.get_title());
                                        params.put(ReqConst.PARAM_DESCRIPTION, question.get_description());
                                        params.put(ReqConst.PARAM_SUBJECTS, question.get_subjects().get(0));

                                        Log.d("Subject==>", question.get_subjects().get(0));

                                    } catch (Exception e){}

                                    return params;
                                }
                            };

                            request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            Q2AApplication.getInstance().addToRequestQueue(request, url);

                    }
                });


                holder.rlt_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                            String url = ReqConst.SERVER_URL + ReqConst.REQ_DELETE_REQUEST;

                            _activity.showProgress();
                            StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String json) {

                                    _activity.closeProgress();

                                    Log.d("delete===response",json);

                                    try {
                                        JSONObject response = new JSONObject(json);

                                        int result_code = response.getInt(ReqConst.RES_CODE);
                                        if (result_code == ReqConst.CODE_SUCCESS){

                                            _activity.gotoHomeFragment();
                                        }
                                    } catch (JSONException e) {
                                        _activity.closeProgress();
                                        _activity.showAlertDialog(_activity.getString(R.string.error));
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    _activity.closeProgress();
                                    _activity.showAlertDialog(_activity.getString(R.string.error));
                                }
                            }){

                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {

                                    Map<String, String> params = new HashMap<>();

                                    try {
                                        params.put(ReqConst.PARAM_ID, String.valueOf(question.get_id()));
                                    } catch (Exception e){}

                                    return params;
                                }
                            };

                            request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            Q2AApplication.getInstance().addToRequestQueue(request, url);

                        }


                });

                holder.lyt_reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        _activity.gotoShowDetails(question);
                    }
                });
            }

            break;
        }

        return convertView;
    }

 /*   public void removeItem(QuestionEntity entity){

        _questions.remove(entity);
    }*/

    public class RequestHolder {

        LinearLayout lyt_request;
        RadiusImageView imv_sub_photo;
        TextView txv_title;
        TextView txv_contents;
        TriangleLabelView label_view;
    }

    public class AcceptedHolder {

        LinearLayout lyt_accepted;
        RadiusImageView imv_sub_photo;
        TextView txv_title;
        TextView txv_contents;
        RelativeLayout rlt_call;
        TriangleLabelView label_view;
    }

    public class RejectedHolder{

        LinearLayout lyt_reject;
        RadiusImageView imv_sub_photo;
        TextView txv_title;
        TextView txv_contents;
        RelativeLayout rlt_delete,rlt_edit, rlt_resend;
        TriangleLabelView label_view;
    }
}
