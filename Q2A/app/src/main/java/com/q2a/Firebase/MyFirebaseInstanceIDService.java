/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.q2a.Firebase;


import android.util.Log;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.q2a.Q2AApplication;
import com.q2a.R;
import com.q2a.activitys.MainActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.preference.PrefConst;
import com.q2a.preference.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    MainActivity _activity;


    /**
     * The Application's current Instance ID token is no longer valid
     * and thus a new one must be requested.
     */
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.d(TAG, "Refreshed token: " + refreshedToken);

        Log.d("========token======", refreshedToken);

        Constants.TOKEN = refreshedToken;

        if (!refreshedToken.toString().equals(null)){

           updateToken();
        }

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    private void sendRegistrationToServer(String token) {

        Preference.getInstance().put(this, PrefConst.PREFKEY_TOKEN, token);
        Constants.TOKEN = Preference.getInstance().getValue(this, PrefConst.PREFKEY_TOKEN, "");
    }

    private void updateToken(){

        //_activity = Constants.mainActivity;

        String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATE_TOKEN;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseMain(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               /* _activity.showAlertDialog(getString(R.string.error));
                _activity.closeProgress();*/
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_ID, String.valueOf(Commons.g_user.get_id()));
                    params.put(ReqConst.PARAM_TOKEN, Constants.TOKEN);

                } catch (Exception e) {
                /*    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.error));*/
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Q2AApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseMain(String json){

        try {

            JSONObject object = new JSONObject(json);

            int result_message = object.getInt(ReqConst.RES_CODE);

            if (result_message ==  ReqConst.CODE_SUCCESS){

            } else {

              /*  _activity.showAlertDialog(String.valueOf(result_message));
                _activity.closeProgress();*/
            }

        } catch (JSONException e) {

            /*_activity.closeProgress();
            _activity.showAlertDialog(_activity.getString(R.string.error));*/
            e.printStackTrace();
        }
    }

}
