package com.q2a.Firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.q2a.R;
import com.q2a.activitys.MainActivity;
import com.q2a.commons.Commons;
import com.q2a.commons.Constants;
import com.q2a.commons.ReqConst;
import com.q2a.model.ImagesEntity;
import com.q2a.model.QuestionEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by aa on 10/14/2016.
 */
public class MyFireBaseMessagingService extends FirebaseMessagingService {

   // MainActivity activity;
    private static final String TAG = "MyFirebaseMsgService";
    private LocalBroadcastManager broadcaster;

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
           // Constants.receivemessage = remoteMessage.getNotification().getBody();
        }
        if (remoteMessage.getNotification() != null) {
            //Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

         //   Log.d(TAG,"======Message Notification Sound" + remoteMessage.getNotification().getSound());

          //  Log.d("mesagesount==========",remoteMessage.getNotification().getSound());

            String sound = remoteMessage.getNotification().getSound();
            String body = remoteMessage.getNotification().getBody();

            if (body.length() > 0){

                parseNoti(body);

            } else Toast.makeText(this, "Failed Noti", Toast.LENGTH_SHORT).show();


            //String body = remoteMessage.getNotification().getBody();

            String[] separated = body.split(" : ");

            ArrayList aList = new ArrayList(Arrays.asList(sound.split(":")));

           // Log.d("messagetype====",aList.get(3).toString().trim());

        /*    if (aList.size() > 0) {

                for (int i = 0; i < aList.size(); i++) {

                if(aList.get(1).toString().trim().equals("1")){

                    Constants.messagetype = 0;
                    //Constants.messagestatuse=false;
                    Constants.recieveimageurl = remoteMessage.getNotification().getBody();
                    Log.d("imageurl========",Constants.recieveimageurl);
                    }

                    Constants.noti_counter = aList.size();

                    Log.d("=====noti_counter=====", String.valueOf(Constants.noti_counter));

                    Constants.messagetype = 0;

                    Log.d("sfsdfsd","sfsdfsd");
                    Constants.messagetype = 0;
                    Constants.messagestatuse = true;
                    Constants.receivemessage = remoteMessage.getNotification().getBody();

                    NotiModel notiModel = new NotiModel();

                    notiModel.setId(Integer.parseInt(aList.get(1).toString()));
                    notiModel.setNoti_type(Integer.parseInt(aList.get(3).toString().trim()));
                    notiModel.setUsername(aList.get(5).toString().trim());
                    String photo_url = aList.get(7).toString().trim();
                    photo_url = photo_url.replace("%999",".");
                    notiModel.setNoti_photo(photo_url);

                    Constants.notiModels.add(notiModel);

                }

            } else {

                Toast.makeText(this, "Null", Toast.LENGTH_SHORT).show();
            }*/

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

    }

    private void parseNoti(String json){

        Constants.NOTI_COUNTER++ ;

        Intent intent = new Intent("firsttap");
        broadcaster.sendBroadcast(intent);

        showNotification("You received a request !");

      /*  if (Constants.SUB_HOME_FRAGMENT != null){

            Constants.SUB_HOME_FRAGMENT.getAllHome();

        } else if (Constants.SUB_NOTI_FRAGMENT != null){

            Constants.SUB_NOTI_FRAGMENT.getAllNotification();

        }  else if (Constants.STUDENT_HOME_FRAGMENT != null){

            Constants.STUDENT_HOME_FRAGMENT.getAllHome();

        } else if (Constants.STUDENT_NOTI_FRAGMENT != null){

            Constants.STUDENT_NOTI_FRAGMENT.getAllNotification();
        }*/

        /*try {

            JSONObject response = new JSONObject(json);

            JSONArray notification = response.getJSONArray(ReqConst.REQ_NOTIFICATION);
            for (int i = 0; i < notification.length(); i++){

                QuestionEntity notiModel = new QuestionEntity() ;
                JSONObject json_noti = (JSONObject)notification.get(i);


                ArrayList<ImagesEntity> _images = new ArrayList<>();
                JSONArray json_noti_images = json_noti.getJSONArray(ReqConst.RES_IMAGES);
                for (int j = 0; j < json_noti_images.length(); j++){

                    JSONObject _image = (JSONObject)json_noti_images.get(j);
                    ImagesEntity imagesEntity = new ImagesEntity();

                    imagesEntity.set_id(_image.getInt(ReqConst.RES_ID));
                    imagesEntity.set_image_url(_image.getString(ReqConst.RES_IMAGE));

                    _images.add(imagesEntity);

                }
                notiModel.set_images_url(_images);
                notiModel.set_thumb_url(json_noti.getString(ReqConst.RES_THUMBNAIL));
                notiModel.set_video_url(json_noti.getString(ReqConst.RES_VIDEO));

                //subject
                ArrayList<String> _subjects = new ArrayList<String>(Arrays.asList(json_noti.getString("subject").split(":")));
                notiModel.set_subjects(_subjects);

                notiModel.set_id(json_noti.getInt(ReqConst.RES_ID));
                notiModel.set_description(json_noti.getString(ReqConst.RES_DESCRIPTION));
                notiModel.set_noti_id(json_noti.getInt(ReqConst.RES_NOTI_ID));
                notiModel.set_status(json_noti.getInt(ReqConst.RES_STATUS));

                //show_other part
                if (Commons.g_user.get_subscriber() == 1){

                    JSONObject json_from = json_noti.getJSONObject("from");
                    notiModel.set_other_id(json_from.getInt(ReqConst.RES_ID));
                    notiModel.set_other_imageUrl(json_from.getString("avatar"));
                    notiModel.set_other_phone(json_from.getString(ReqConst.RES_PHONE));

                } else {

                    JSONObject json_from = json_noti.getJSONObject("to");
                    notiModel.set_other_id(json_from.getInt(ReqConst.RES_ID));
                    notiModel.set_other_imageUrl(json_from.getString("avatar"));
                    notiModel.set_other_phone(json_from.getString(ReqConst.RES_PHONE));
                }

                Log.d("======MessageCouter", String.valueOf(Constants.NOTI_COUNTER));
                Log.d("=====Id ===", String.valueOf(notiModel.get_id()));
                Log.d("=====messagetype ===", String.valueOf(notiModel.get_status()));
                Log.d("=====Id ===", notiModel.get_description());
                Log.d("=====Id ===", (notiModel.get_other_imageUrl()));

                //Constants.notiModels.add(notiModel);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }*/

    }

    private void showNotification(String msg) {

        //Constants.noti_status = 1;

        Intent intent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(msg)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 , notificationBuilder.build());

        //---------------------
        Intent _intent = new Intent("my-event");
        LocalBroadcastManager.getInstance(this).sendBroadcast(_intent);

    }
}
