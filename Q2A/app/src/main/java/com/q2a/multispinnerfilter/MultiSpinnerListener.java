package com.q2a.multispinnerfilter;

public interface MultiSpinnerListener {
    void onItemsSelected(boolean[] selected);
}